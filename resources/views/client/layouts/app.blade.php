<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @include('client.partials.styles')
    @yield('client.style')

    @toastr_css
    <title>B.I.C</title>
</head>

<body>

    @include('client.partials.header')

    @yield('client.content')

    @include('client.partials.footer')

    @include('client.partials.scripts')
    @yield('client.script')

    @toastr_js
    @toastr_render
</body>

</html>