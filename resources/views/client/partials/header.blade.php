<header class="header clearfix">
    <div class="header-inner">
        <nav class="navbar navbar-expand-lg bg-micko micko-head justify-content-sm-start micko-top pt-0 pb-0">
            <div class="container">
                <button class="navbar-toggler align-self-start" type="button">
                    <span class="navbar-toggler-icon"><i class="fas fa-bars"></i></span>
                </button>
                <a class="navbar-brand order-1 order-lg-0 ml-lg-0 ml-2 me-4" href="{{ route('client.accueil.index') }}">
                    <div class="res_main_logo">
                        <img src="{{ asset('assets/images/res-logo.png') }}" alt="">
                    </div>
                    <div class="main_logo" id="logo">
                        <img src="{{ asset('assets/images/logo.png') }}" alt="">
                        <img class="logo-inverse" src="{{ asset('assets/images/dark-logo.png') }}" alt="">
                    </div>
                </a>
                <div class="collapse navbar-collapse bg-micko-nav p-3 p-lg-0 mt-6 mt-lg-0 d-flex flex-column flex-lg-row flex-xl-row justify-content-lg-start mobileMenu" id="navbarSupportedContent">
                    <ul class="navbar-nav align-self-stretch">
                        <li class="nav-item">
                            <a class="nav-link active fw-bolder" href="{{ route('client.accueil.index') }}">Accueil</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link fw-bolder" href="{{ route('client.publications.index') }}">Publications</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link fw-bolder" href="#">Categories</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link fw-bolder" href="{{ route('client.client.apropos') }}">A propos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link fw-bolder" href="{{ route('client.client.faq') }}">FAQ</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link fw-bolder" href="{{ route('client.client.contact') }}">Contact</a>
                        </li>
                    </ul>
                </div>
                <div class="msg-noti-acnt-section order-2">
                    <ul class="mn-icons-set align-self-stretch">
                        @auth('front')
                        <li class="mn-icon dropdown dropdown-account">
                            <a class="mn-link" href="#" role="button">
                                <i class="fas fa-bell"></i>
                                <div class="alert-circle"></div>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-account dropdown-menu-end">
                                <li class="media-list">
                                    <div class="media">
                                        <div class="media-body">
                                            Bonjour Madame
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        
                        <li class="mn-icon dropdown dropdown-account">
                            <div class="opts_account" >
                                <h5 class="d-inline text-uppercase">
                                    {{ auth('front')->user()->pseudo }}
                                </h5>
                                <i class="fas fa-caret-down arrow-icon"></i>
                            </div>
                            <ul class="dropdown-menu dropdown-menu-account dropdown-menu-end">
                                <li class="media-list">
                                    <div class="media">
                                        <div class="media-left">
                                            <img class="ft-plus-square icon-bg-circle bg-cyan mr-0" src="{{ asset('assets/images/left-imgs/img-1.jpg') }}" alt="">
                                        </div>
                                        <div class="media-body">
                                            <h6 class="media-heading text-16">
                                                {{ auth('front')->user()->prenom . ' ' . auth('front')->user()->nom }}
                                            </h6>
                                        </div>
                                    </div>
                                </li>
                                <li class="dropdown-menu-footer">
                                    <a class="dropdown-item-link text-link text-center" href="{{ route('client.users.show', auth('front')->user()) }}">
                                        Profil
                                    </a>
                                    <form action="{{ route('client.logout') }}" method="post">
                                        @csrf
                                        <button type="submit" class="dropdown-item-link text-link text-center" >
                                            <i class="fa fa-sign-out"></i>
                                            <span>Déconnexion</span>
                                        </button>
                                    </form>
                                </li>
                            </ul>
                        </li>
                        @else
                        <a href="{{ route('client.login') }}" class="btn-login btn-hover px-3 py-2 btn-rounded d-inline" >
                            <h5 class="d-inline">Connexion</h5>
                        </a>
                        @endauth
                    </ul>
                </div>
            </div>
        </nav>
        <div class="overlay"></div>
    </div>
</header>