<footer class="footer">
    <div class="footer-bottom-items">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="footer-bottom-links">
                        <div class="footer-logo mb-3" id="logo-footer">
                            <a href="#"><img src="{{ asset('assets/images/logo.png') }}" alt=""></a>
                        </div>
                        <div class="micko-copyright">
                            <p>
                                <i class="fas fa-copyright"></i>
                                Copyright {{ now()->year }} B.I.C par Elite Group SARL. Tous droits reservés.</p>
                        </div>
                        <ul class="social-ft-links">
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-whatsapp"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>