@extends('client.layouts.app')

@section('client.content')

@include('Publication::create')

<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12">

                <div class="all-items">
                    <div class="product-items-list">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="learning_all_items mt-30">
                                    <div class="owl_title d-flex justify-content-between">
                                        <h4>Trending</h4>

                                        <div class = "">
                                            <button class="apply_job_btn ps-4 pe-4 view-btn btn-hover">Ajouter une annonce</button>
                                        </div>

                                    </div>
                                    <div class="owl-carousel learning_slider owl-theme">

                                        <div class="item">
                                            <div class="full-width">
                                                <div class="recent-items">
                                                    <div class="posts-list">
                                                        <div class="feed-shared-product-dt">
                                                            <div class="pdct-img crse-img-tt">
                                                                <a href="course_detail_view.html">
                                                                <img class="ft-plus-square product-bg-w bg-cyan me-0" src="assets/images/courses-imgs/img-6.jpg" alt="">
                                                                <div class="overlay-item">
                                                                    <div class="badge-level trnd-clr">Trending</div>
                                                                    <div class="badge-timer">5 hours</div>
                                                                </div>
                                                                </a>
                                                            </div>
                                                            <div class="author-dts pp-20">
                                                                <a href="course_detail_view.html" class="job-heading pp-title">Angular 8 - The Complete Guide (2020 Edition)</a>
                                                                <p class="notification-text font-small-4">
                                                                by <a href="#" class="cmpny-dt blk-clr">Amritpal Singh</a>
                                                                </p>
                                                                <p class="notification-text font-small-4 pt-1 catey-group">
                                                                <a href="#" class="catey-dt">Development</a>
                                                                <a href="#" class="catey-sub">Angular</a>
                                                                </p>
                                                                <div class="ppdt-price-sales">
                                                                    <div class="ppdt-price">
                                                                    $15
                                                                    </div>
                                                                    <div class="ppdt-sales">
                                                                    32 Learners
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="post-meta">
                                                    <div class="job-actions">
                                                        <div class="aplcnts_15">
                                                            <a href="course_detail_view.html" class="view-btn btn-hover">Detail View</a>
                                                        </div>
                                                        <div class="action-btns-job">
                                                            <a href="#" class="crt-btn crt-btn-hover me-2"><i class="fas fa-shopping-cart"></i></a>
                                                            <a href="#" class="bm-btn bm-btn-hover"><i class="far fa-bookmark"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="learning_all_items mt-30">
                                    <div class="owl_title d-flex justify-content-between">
                                        <h4>Trending</h4>
                                        <div class = "">
                                            <button class="apply_job_btn ps-4 pe-4 view-btn btn-hover">Ajouter une annonce</button>
                                        </div>
                                        
                                    </div>
                                    <div class="owl-carousel learning_slider owl-theme">

                                        <div class="item">
                                            <div class="full-width">
                                                <div class="recent-items">
                                                    <div class="posts-list">
                                                        <div class="feed-shared-product-dt">
                                                            <div class="pdct-img crse-img-tt">
                                                                <a href="course_detail_view.html">
                                                                <img class="ft-plus-square product-bg-w bg-cyan me-0" src="assets/images/courses-imgs/img-6.jpg" alt="">
                                                                <div class="overlay-item">
                                                                    <div class="badge-level trnd-clr">Trending</div>
                                                                    <div class="badge-timer">5 hours</div>
                                                                </div>
                                                                </a>
                                                            </div>
                                                            <div class="author-dts pp-20">
                                                                <a href="course_detail_view.html" class="job-heading pp-title">Angular 8 - The Complete Guide (2020 Edition)</a>
                                                                <p class="notification-text font-small-4">
                                                                by <a href="#" class="cmpny-dt blk-clr">Amritpal Singh</a>
                                                                </p>
                                                                <p class="notification-text font-small-4 pt-1 catey-group">
                                                                <a href="#" class="catey-dt">Development</a>
                                                                <a href="#" class="catey-sub">Angular</a>
                                                                </p>
                                                                <div class="ppdt-price-sales">
                                                                    <div class="ppdt-price">
                                                                    $15
                                                                    </div>
                                                                    <div class="ppdt-sales">
                                                                    32 Learners
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="post-meta">
                                                    <div class="job-actions">
                                                        <div class="aplcnts_15">
                                                            <a href="course_detail_view.html" class="view-btn btn-hover">Detail View</a>
                                                        </div>
                                                        <div class="action-btns-job">
                                                            <a href="#" class="crt-btn crt-btn-hover me-2"><i class="fas fa-shopping-cart"></i></a>
                                                            <a href="#" class="bm-btn bm-btn-hover"><i class="far fa-bookmark"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="learning_all_items mt-30">
                                    <div class="owl_title d-flex justify-content-between">
                                        <h4>Trending</h4>
                                        <div class = "">
                                            <button class="apply_job_btn ps-4 pe-4 view-btn btn-hover">Ajouter une annonce</button>
                                        </div>
                                        
                                    </div>
                                    <div class="owl-carousel learning_slider owl-theme">

                                        <div class="item">
                                            <div class="full-width">
                                                <div class="recent-items">
                                                    <div class="posts-list">
                                                        <div class="feed-shared-product-dt">
                                                            <div class="pdct-img crse-img-tt">
                                                                <a href="course_detail_view.html">
                                                                <img class="ft-plus-square product-bg-w bg-cyan me-0" src="assets/images/courses-imgs/img-6.jpg" alt="">
                                                                <div class="overlay-item">
                                                                    <div class="badge-level trnd-clr">Trending</div>
                                                                    <div class="badge-timer">5 hours</div>
                                                                </div>
                                                                </a>
                                                            </div>
                                                            <div class="author-dts pp-20">
                                                                <a href="course_detail_view.html" class="job-heading pp-title">Angular 8 - The Complete Guide (2020 Edition)</a>
                                                                <p class="notification-text font-small-4">
                                                                by <a href="#" class="cmpny-dt blk-clr">Amritpal Singh</a>
                                                                </p>
                                                                <p class="notification-text font-small-4 pt-1 catey-group">
                                                                <a href="#" class="catey-dt">Development</a>
                                                                <a href="#" class="catey-sub">Angular</a>
                                                                </p>
                                                                <div class="ppdt-price-sales">
                                                                    <div class="ppdt-price">
                                                                    $15
                                                                    </div>
                                                                    <div class="ppdt-sales">
                                                                    32 Learners
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="post-meta">
                                                    <div class="job-actions">
                                                        <div class="aplcnts_15">
                                                            <a href="course_detail_view.html" class="view-btn btn-hover">Detail View</a>
                                                        </div>
                                                        <div class="action-btns-job">
                                                            <a href="#" class="crt-btn crt-btn-hover me-2"><i class="fas fa-shopping-cart"></i></a>
                                                            <a href="#" class="bm-btn bm-btn-hover"><i class="far fa-bookmark"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>






    
@endsection