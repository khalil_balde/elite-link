<div class="deznav">
    <div class="deznav-scroll">
        
        <div class="main-profile">
            <div class="image-bx user-card mb-2">
                <div class="dz-media rounded-circle mr-3">
                    <span class="icon-placeholder bgl-success text-sucess">{{ Str::substr(auth()->user()->prenom, 0, 1) . Str::substr(auth()->user()->nom, 0, 1) }}</span>
                </div>
            </div>
            <h5 class="name"><span class="font-w400">Bonjour,</span> {{ auth()->user()->pseudo }}</h5>
            <p class="email">{{ auth()->user()->email }}</p>
        </div>
        
        <ul class="metismenu" id="menu">
            <li class="nav-label first">Menu</li>

            @include('Dashboard::Menu')
            @include('Entreprise::menu')
            @include('Tarification::menu')
            @include('Categorie::menu')
            @include('Client::menu')
            @include('Publication::menu')
            @include('Offre::menu')
            @include('Faq::menu')
            @include('User::menu')

            </li>
        </ul>
    </div>
</div>