<?php

namespace App\Http\Controllers;

use Ramsey\Uuid\Uuid;
use Intervention\Image\Facades\Image;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    // public function uploadImage($image)
    // {
    //     $imageName = Uuid::uuid4()->toString();
    //     Image::make($image)->resize(150, 150, function ($constraint) {
    //         $constraint->aspectRatio();
    //     })->save('storage/'. $imageName);
    //     return $imageName;
    // }
}
