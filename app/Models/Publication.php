<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Publication extends Model
{
    use HasFactory;
    // public $preventsLazyLoading = true;

    protected $guarded = [];

    protected function dateIntervention(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Carbon::parse($value)->format('d-m-Y')
        );
    }

    protected function prixMin(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => number_format($value, 0, ',', ' '),
        );
    }

    protected function prixMax(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => number_format($value, 0, ',', ' '),
        );
    }

    protected function vues(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $value . ' ' . Str::plural('vue', $value)
        );
    }

    protected function offresCount(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $this->offres()->count() . ' ' . Str::plural('offre', $value)
        );
    }

    /**
     * Get the client that owns the Tache
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client(): BelongsTo
    {
        return $this->belongsTo(Client::class);
    }

    /**
     * Get the categorie that owns the Tache
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function categorie(): BelongsTo
    {
        return $this->belongsTo(Categorie::class);
    }

    /**
     * Get all of the offres for the Tache
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function offres(): HasMany
    {
        return $this->hasMany(Offre::class);
    }

}
