<?php

namespace App\Models;

use Laravel\Sanctum\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Client extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $guarded = [];

    protected $hidden = [
        'password',
    ];

    public function fullName()
    {
        return $this->prenom. ' ' . $this->nom;
    }

    /**
     * Get all of the publications for the Client
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function publications(): HasMany
    {
        return $this->hasMany(Publication::class);
    }

    /**
     * Get all of the offres for the Client
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function offres(): HasMany
    {
        return $this->hasMany(Offre::class);
    }
}
