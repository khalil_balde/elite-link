<?php

namespace App\Admin\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Client;
use App\Models\Offre;
use App\Models\Publication;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $data['clients'] = Client::query()->count();
        $data['pub'] = Publication::query()->count();
        $data['users'] = Admin::query()->count();
        $data['offres'] = Offre::query()->count();
        return view('Dashboard::index', compact('data'));
    }
}
