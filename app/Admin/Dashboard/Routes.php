<?php

use App\Admin\Dashboard\DashboardController;

Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');