@extends('admin.layouts.app')

@section('content')
<div class="container-fluid">
  
  <div class="row">
    <div class="col-xl-3 col-sm-6 m-t35">
      <div class="card card-coin">
        <div class="card-body text-center">
          <h4>Nombre de clients</h4>
          <h2 class="text-black mb-2 font-w600">{{ $data['clients'] }}</h2>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-sm-6 m-t35">
      <div class="card card-coin">
        <div class="card-body text-center">
          <h4>Nombre de publications</h4>
          <h2 class="text-black mb-2 font-w600">{{ $data['pub'] }}</h2>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-sm-6 m-t35">
      <div class="card card-coin">
        <div class="card-body text-center">
          <h4>Nombre d'utilisateurs</h4>
          <h2 class="text-black mb-2 font-w600">{{ $data['users'] }}</h2>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-sm-6 m-t35">
      <div class="card card-coin">
        <div class="card-body text-center">
          <h4>Nombre d'offres</h4>
          <h2 class="text-black mb-2 font-w600">{{ $data['offres'] }}</h2>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection