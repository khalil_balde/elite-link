<div class="modal fade" id="details_client_modal-{{ $client->id }}"">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Détails du client</h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
               <div class="row">
                    <div class="col-12">
                        <div class="card user-card">
                            <div class="card-body pb-0">
                                <div class="d-flex mb-3 align-items-center">
                                    <div class="dz-media rounded-circle mr-3">
                                        <span class="icon-placeholder bgl-{{ Arr::random(['success', 'danger', 'warning', 'primary', 'secondary']) }} text-{{ Arr::random(['success', 'danger', 'warning', 'primary', 'secondary']) }}">{{ Str::substr($client->prenom, 0, 1) . Str::substr($client->nom, 0, 1) }}</span>
                                    </div>
                                    <div>
                                        <h5 class="title"><a href="javascript:void(0);">{{ $client->prenom . ' ' . $client->nom }}</a></h5>
                                        @if ($client->publications->count() !== 0)
                                            <div class="mt-3">
                                                <span class="badge badge-circle badge-light">{{ $client->publications->count() }} tâches postées</span>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">
                                        <span class="mb-0 title">Email</span> :
                                        <span class="text-black ml-2">{{ $client->email }}</span>
                                    </li>
                                    <li class="list-group-item">
                                        <span class="mb-0 title">Téléphone</span> :
                                        <span class="text-black ml-2">{{ $client->telephone }}</span>
                                    </li>
                                    <li class="list-group-item">
                                        <span class="mb-0 title">Adresse</span> :
                                        <span class="text-black desc-text ml-2">{{ $client->adresse }}</span>
                                    </li>
                                    <li class="list-group-item">
                                        <span class="mb-0 title">Pseudo</span> :
                                        <span class="text-black desc-text ml-2">{{ $client->pseudo }}</span>
                                    </li>
                                    <li class="list-group-item">
                                        <span class="mb-0 title">Satut</span> :
                                        @if ($client->status)
                                            <span class="badge badge-success">Actif</span>
                                        @else
                                        <span class="badge badge-danger">Suspendu</span>
                                        @endif
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
               </div>
            </div>
        </div>
    </div>
</div>