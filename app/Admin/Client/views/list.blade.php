@extends('admin.layouts.app')

@section('style')
    <link href="{{ asset('vendor/datatables/css/jquery.dataTables.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-toggle.min.css') }}">
@endsection

@section('content')

<div class="col-12">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Listes des Clients</h4>
        </div>
        <div class="card-body">
            <div class="">
                <table id="example4" class="display">
                    <thead>
                        <tr>
                            <th style="font-weight: bold">#</th>
                            <th style="font-weight: bold">Prenom</th>
                            <th style="font-weight: bold">Nom</th>
                            <th style="font-weight: bold">Email</th>
                            <th style="font-weight: bold">Telephone</th>
                            <th style="font-weight: bold">Statut</th>
                            <th style="font-weight: bold">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($clients as $client)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $client->prenom }}</td>
                                <td>{{ $client->nom }}</td>
                                <td>{{ $client->email }}</td>
                                <td>{{ $client->telephone }}</td>
                                <td>
                                    @if ($client->status)
                                        <span class="badge badge-success">Actif</span>
                                    @else    
                                        <span class="badge badge-danger">Suspendu</span>
                                    @endif
                                </td>
                                <td>
                                    <button type="button" class="btn btn-info btn-rounded btn-sm mb-2" data-toggle="modal" data-target="#details_client_modal-{{ $client->id }}">
                                        <i class="fa fa-eye mr-2"></i>
                                        <span>Détails</span>
                                    </button>
                                    @include('Client::details')
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
    <script src="{{ asset('vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins-init/datatables.init.js') }}"></script>

@endsection