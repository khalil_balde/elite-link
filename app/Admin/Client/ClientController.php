<?php

namespace App\Admin\Client;

use App\Http\Controllers\Controller;
use App\Models\Client;

class ClientController extends Controller
{
    public function index()
    { 
        $clients = Client::with(['publications', 'offres'])->latest()->get();
        return view('Client::list', compact('clients'));
    }

}
