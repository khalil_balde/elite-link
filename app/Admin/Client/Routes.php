<?php

use App\Admin\Client\ClientController;

Route::resource('clients', ClientController::class);