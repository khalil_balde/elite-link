<?php

namespace App\Admin\Entreprise;

use App\Http\Controllers\Controller;
use App\Models\Information;
use Illuminate\Http\Request;

class EntrepriseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $information = Information::first();
        return  view('Entreprise::index', compact('information'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $information = Information::first();
        
        $image = $this->uploadImage($request->image);

        if($information){

            $information->nom = $request->nom;
            $information->slogant = $request->slogant;
            $information->siege = $request->siege;
            $information->email = $request->email;
            $information->pays = $request->pays;
            $information->ouvrable = $request->ouvrable;
            $information->image = $image ;
            $information->footer = $request->footer;
            $information->contact = $request->contact;
            $information->description = $request->description;
            $information->about = $request->about;
        
            $information->save();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
