@extends('admin.layouts.app')

@section('content')

    <div class="col-xl-12 col-lg-12 ">
        <div class="card mt-30">
            <div class="card-header">
                <h4 class="card-title">Informatin de L'entreprise</h4>
            </div>
            <div class="card-body">
                <div class="basic-form">
                    <form action="{{ route('admin.entreprise.store') }}" method="post">
                    @csrf
                        <div class="row">
                            <div class="form-group col-md-4">
                            <label class="">nom</label>
                                <input class="form-control form-control" type="text" name="nom" value="{{ $information->nom }}">
                            </div>
                            <div class="form-group col-md-4">
                            <label class="">Slogant</label>
                                <input class="form-control form-control" type="text" name="slogant" value="{{ $information->slogant }}">
                            </div>
                            <div class="form-group col-md-4">
                            <label class="">Siege</label>
                                <input class="form-control form-control-sm" type="text" name="siege" value="{{ $information->siege }}">
                            </div>
                            <div class="form-group col-md-12">
                            <label class="">Email</label>
                                <input class="form-control form-control-sm" type="email" name="email" value="{{ $information->email }}">
                            </div>
                            <div class="form-group col-md-4">
                            <label class="">Pays</label>
                                <input class="form-control form-control-sm" type="text" name="pays" value="{{ $information->pays }}">
                            </div>
                            <div class="form-group col-md-4">
                            <label class="">Ouvrable</label>
                                <input class="form-control form-control-sm" type="text" name="ouvrable" value="{{ $information->ouvrable }}">
                            </div>
                            <div class="form-group col-md-4">
                            <label class="">Image</label>
                                <div class="input-group mb-3">
                                        <div class="custom-file">
                                            <input type="file" name="image" value=""class="custom-file-input">
                                            <label class="custom-file-label">Choose file</label>
                                        </div>
                                        <div class="input-group-append">
                                            <span class="input-group-text">Upload</span>
                                        </div>
                                    </div>
                            </div>
                            <div class="form-group col-md-6">
                            <label class="">Footer</label>
                                <input class="form-control form-control-sm" type="text" name="footer" value="{{ $information->footer }}">
                            </div>
                            <div class="form-group col-md-6">
                            <label class="">Contact</label>
                                <input class="form-control form-control-sm" type="text" name="contact" value="{{ $information->contact }}">
                            </div>
                            <div class="form-group col-md-6">
                            <label class="">Description</label>
                                <textarea class="form-control form-control-sm" type="text" rows="5" name="description">{{ $information->description }}</textarea>
                            </div>
                            <div class="form-group col-md-6">
                            <label class="">A propos</label>                        
                                <textarea class="form-control form-control-sm" type="text" rows="5" name="about">{{ $information->about }}</textarea>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Enregistrer</button>
                    </form>
                </div>
            </div>
        </div>
    </div>



@stop