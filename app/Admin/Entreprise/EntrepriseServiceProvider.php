<?php

namespace App\Admin\Entreprise;

use Illuminate\Support\ServiceProvider;

class EntrepriseServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsfrom(__DIR__.'/views', 'Entreprise');
    }
}
