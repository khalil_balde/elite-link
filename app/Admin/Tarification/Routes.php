<?php

use App\Admin\Tarification\TarificationController;

Route::resource('tarifications', TarificationController::class);