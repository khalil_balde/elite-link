@extends('admin.layouts.app')

@section('style')
<link href="{{ asset('vendor/datatables/css/jquery.dataTables.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('css/bootstrap-toggle.min.css') }}">
@endsection

@section('content')

<div class="col-12">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Liste des tarifications</h4>
            <button type="button" class="btn btn-primary btn-rounded mb-2" data-toggle="modal"
                data-target="#create_tarification_modal">
                <i class="fa fa-plus-circle mr-2"></i>
                <span>Ajouter une tarification</span>
            </button>
            @include('Tarification::create')
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="example4" class="display">
                    <thead>
                        <tr>
                            <th style="font-weight: bold">#</th>
                            <th style="font-weight: bold">Type</th>
                            <th style="font-weight: bold">Nombre</th>
                            <th style="font-weight: bold">Prix (GNF)</th>
                            <th style="font-weight: bold">Expiration</th>
                            <th style="font-weight: bold">Statut</th>
                            <th style="font-weight: bold">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($tarifications as $tarification)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $tarification->type }}</td>
                            <td>{{ $tarification->nombre }}</td>
                            <td>{{ $tarification->prix }}</td>
                            <td>{{ $tarification->validite }}</td>
                            <td>
                                <input type="checkbox" data-id="{{ $tarification->id }}" class="toggle-class"
                                    data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="ACFTIF"
                                    data-off="SUSPENDU" {{ $tarification->status ? 'checked' : '' }}>
                            </td>
                            <td>
                                <button type="button" class="btn btn-info btn-rounded btn-sm mb-2" data-toggle="modal"
                                    data-target="#edit_tarification_modal-{{ $tarification->id }}">
                                    <i class="fa fa-edit mr-2"></i>
                                    <span>Editer</span>
                                </button>
                                @include('Tarification::edit')
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script src="{{ asset('vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/plugins-init/datatables.init.js') }}"></script>
<script src="{{ asset('js/bootstrap-toggle.min.js') }}"></script>
<script>
    $(function () {
            $('.toggle-class').change(function () {
                 var status = $(this).prop('checked') == true ? 1 : 0;
                 var tarification_id = $(this).data('id');
                 $.ajax({
                     type: 'GET',
                     dataType: 'JSON',
                     url: '/tarifications-change-status',
                     data: {'status': status, 'tarification_id': tarification_id},
                     success: function (data) {
                        console.log('Success');
                     }
                 });
            });
        });
</script>
@endsection