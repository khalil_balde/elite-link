<!-- Modal -->
<div class="modal fade" id="create_tarification_modal">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ajout d'une tarification</h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                </button>
            </div>
            <form action="{{ route('admin.tarifications.store') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="form-group mb-4">
                        <label for="type"><strong>Type</strong></label>
                        <select name="type" id="type">
                            <option value="Post">Post</option>
                            <option value="Soumission">Soumission</option>
                        </select>
                    </div>
                    <div class="form-group mb-4">
                        <label for="nombre"><strong>Nombre</strong></label>
                        <input type="text" name="nombre" id="nombre" class="form-control form-control-lg"
                            value="{{ old('nombre') }}" required>
                    </div>
                    <div class="form-group mb-4">
                        <label for="prix"><strong>Prix</strong></label>
                        <input type="text" name="prix" id="prix" class="form-control form-control-lg"
                            value="{{ old('prix') }}" required>
                    </div>
                    <div class="form-group mb-4">
                        <label for="validite"><strong>Date d'expiration</strong></label>
                        <input type="date" name="validite" id="validite" class="form-control form-control-lg"
                            value="{{ old('validite') }}" required>
                    </div>
                    <div class="form-group mb-4">
                        <label for="essai"><strong>Essai</strong></label>
                        <input type="checkbox" name="essai" id="essai">
                    </div>
                    <div class="form-group mb-4">
                        <label for="redondance"><strong>Redondence</strong></label>
                        <input type="checkbox" name="redondance" id="redondance">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded light" data-dismiss="modal">Fermer</button>
                    <button type="submit" class="btn btn-info btn-rounded">
                        <i class="fa fa-save mr-2"></i>
                        Enregistrer
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>