<?php

namespace App\Admin\Tarification;

use Illuminate\Support\ServiceProvider;

class TarificationServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . "/views", "Tarification");
    }
}
