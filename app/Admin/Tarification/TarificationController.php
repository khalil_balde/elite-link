<?php

namespace App\Admin\Tarification;

use App\Http\Controllers\Controller;
use App\Models\Tarification;
use Illuminate\Http\Request;

class TarificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tarifications = Tarification::latest()->paginate(10);
        return view('Tarification::list', compact('tarifications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Tarification::list');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Tarification::query()->create([
            'type' => $request->type,
            'nombre' => $request->nombre,
            'prix' => $request->prix,
            'validite' => $request->validite,
            'essai' => $request->has('essai') ? 1 : 0,
            'essai' => $request->has('redondance') ? 1 : 0,
        ]);
        toastr()->info('La tarification a été ajouté avec succès !!!');
        return to_route('admin.tarifications.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tarification  $tarification
     * @return \Illuminate\Http\Response
     */
    public function show(Tarification $tarification)
    {
        if ($tarification) {
            return view('Tarification::show', compact('tarification'));
        } else {
            toastr()->error('La tarification selectionnée n\'existe pas !');
            return back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tarification  $tarification
     * @return \Illuminate\Http\Response
     */
    public function edit(Tarification $tarification)
    {
        if ($tarification) {
            return view('Tarification::edit', compact('tarification'));
        } else {
            toastr()->error('La tarification selectionnée n\'existe pas !');
            return back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tarification  $tarification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tarification $tarification)
    {
        if ($tarification) {
            $tarification->update([
                'type' => $request->type,
                'nombre' => $request->nombre,
                'prix' => $request->prix,
                'validite' => $request->validite,
                'essai' => $request->has('essai') ? 1 : 0,
                'essai' => $request->has('redondance') ? 1 : 0,
            ]);
            toastr()->info('La tarification a été ajouté avec succès !!!');
            return to_route('admin.tarifications.index');
        } else {
            toastr()->error('La tarification selectionnée n\'existe pas !');
            return back();
        }
        
    }
}
