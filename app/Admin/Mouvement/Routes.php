<?php

use App\Admin\Mouvement\MouvementController;

Route::resource("mouvements", MouvementController::class);