<?php

namespace App\Admin\Mouvement;

use Illuminate\Support\ServiceProvider;

class MouvementServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsfrom(__DIR__.'/views', 'Mouvement');
    }
}
