<?php

namespace App\Admin\Publication;

use App\Models\Publication;
use App\Http\Controllers\Controller;

class PublicationController extends Controller
{
    public function index()
    {
        $publications = Publication::latest()->paginate(10);
        return view('Publication::list', compact('publications'));
    }
}
