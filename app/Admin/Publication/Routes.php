<?php

use App\Admin\Publication\PublicationController;

Route::resource("taches", PublicationController::class)
    ->scoped(['tach' => 'slug']);
