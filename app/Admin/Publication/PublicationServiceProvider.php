<?php

namespace App\Admin\Publication;

use Illuminate\Support\ServiceProvider;

class PublicationServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsfrom(__DIR__.'/views', 'Publication');
    }
}
