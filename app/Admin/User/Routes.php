<?php

use App\Admin\User\UserController;

Route::put('users/{user}/update-password', [UserController::class, 'updatePassword'])
        ->name('users.update-password');

Route::resource('users', UserController::class);

