<?php

namespace App\Admin\User;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = Admin::latest()->paginate(10);
        return view('User::list', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('User::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Admin::query()->create([
            'nom' => $request->nom,
            'prenom' => $request->prenom,
            'email' => $request->email,
            'pseudo' => $request->pseudo,
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        ]);

        toastr()->info('L\'utilisateur a été ajouté avec succès !!!');
        return to_route('admin.users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show(Admin $user)
    {
        if ($user) {
            return view('User::details', compact('user'));
        } else {
            toastr()->error('L\'utilisateur selectionné n\'existe pas !');
            return back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $user)
    {
        if ($user) {
            return view('User::edit', compact('user'));
        } else {
            toastr()->error('L\'utilisateur selectionné n\'existe pas !');
            return back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $user)
    {
        if ($user) {
            $user->update([
                'nom' => $request->nom,
                'prenom' => $request->prenom,
                'email' => $request->email,
                'pseudo' => $request->pseudo,
            ]);
    
            toastr()->info('L\'utilisateur a été modifié avec succès !!!');
            return to_route('admin.users.index');
        } else {
            toastr()->error('L\'utilisateur selectionné n\'existe pas !');
            return back();
        }
    }

    public function changeStatus(Request $request)
    {
        $user = Admin::query()->find($request->user_id);
        if ($user) {
            $user->status = $request->status;
            $user->save();

            toastr()->success('Le status de l\'utilisateur a été mise à jour !');
            return back();
        } else {
            toastr()->error('L\'utilisateur a selectionné n\'existe pas !');
            return back();
        }
    }

    public function updatePassword(Request $request, Admin $user)
    {
        if ($user) {
            $this->validate($request, [
                'current_password' => 'required',
                'new_password' => 'required|confirmed'
            ]);
    
            if (Hash::check($request->current_password, $user->password)) {
                $user->password = Hash::make($request->new_password);
                $user->save();
            } else {
                toastr()->error('Le mot de passe est incorrect !');
                return to_route('admin.users.show', compact('user'));
            }
        } else {
            toastr()->error('L\'utilisateur a selectionné n\'existe pas !');
            return to_route('admin.users.index');
        }
        
        
    }
}
