<div class="modal fade" id="user_modal_add">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ajout d'un utilisateur</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span>&times;</span>
                </button>
            </div>
            <form action="{{ route('admin.users.store') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                      <label for="nom">Nom</label>
                      <input type="text" name="nom" id="nom" class="form-control" value="{{ old('nom') }}">
                    </div>
                    <div class="form-group">
                      <label for="prenom">Prenom</label>
                      <input type="text" name="prenom" id="prenom" class="form-control" value="{{ old('prenom') }}">
                    </div>
                    <div class="form-group">
                      <label for="email">Email</label>
                      <input type="email" name="email" id="email" class="form-control" value="{{ old('email') }}">
                    </div>
                    <div class="form-group">
                      <label for="pseudo">Pseudo</label>
                      <input type="text" name="pseudo" id="pseudo" class="form-control" value="{{ old('pseudo') }}">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-rounded">
                        <i class="fa fa-save mr-2"></i>
                        <span>Enregistrer</span>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>