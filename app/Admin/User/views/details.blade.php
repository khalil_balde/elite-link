@extends('admin.layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-7 mr-2">
            <div class="card">
                <div class="card-body">
                    <div class="profile-tab">
                        <div class="custom-tab-1">
                            <ul class="nav nav-tabs">
                                <li class="nav-item"><a href="#about-me" data-toggle="tab" class="nav-link">Informations</a>
                                </li>
                                @if (auth()->user()->id === $user->id)
                                    <li class="nav-item"><a href="#profile-settings" data-toggle="tab" class="nav-link">Modifier mes infos</a>
                                    </li>
                                @endif
                            </ul>
                            <div class="tab-content">
                                <div id="about-me" class="tab-pane fade active show mt-4">
                                    <div class="profile-personal-info">
                                        <div class="row mb-2">
                                            <div class="col-sm-3 col-5">
                                                <h5 class="f-w-500">Nom <span class="pull-right">:</span>
                                                </h5>
                                            </div>
                                            <div class="col-sm-9 col-7"><span>{{ $user->prenom . ' ' . $user->nom }}</span>
                                            </div>
                                        </div>
                                        <div class="row mb-2">
                                            <div class="col-sm-3 col-5">
                                                <h5 class="f-w-500">Email <span class="pull-right">:</span>
                                                </h5>
                                            </div>
                                            <div class="col-sm-9 col-7"><span>{{ $user->email }}</span>
                                            </div>
                                        </div>
                                        <div class="row mb-2">
                                            <div class="col-sm-3 col-5">
                                                <h5 class="f-w-500">Pseudo <span class="pull-right">:</span></h5>
                                            </div>
                                            <div class="col-sm-9 col-7"><span>{{ $user->pseudo }}</span>
                                            </div>
                                        </div>
                                        <div class="row mb-2">
                                            <div class="col-sm-3 col-5">
                                                <h5 class="f-w-500">Statut <span class="pull-right">:</span></h5>
                                            </div>
                                            <div class="col-sm-9 col-7">
                                                @if ($user->status)
                                                    <span class="badge badge-success badge-rounded">ACTIF</span>
                                                @else
                                                    <span class="badge badge-danger badge-rounded">SUSPENDU</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="profile-settings" class="tab-pane fade">
                                    <div class="pt-3">
                                        <form action="{{ route('admin.users.update', $user) }}" method="POST">
                                            @csrf @method('PUT')
                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                  <label for="nom">Nom</label>
                                                  <input type="text" name="nom" id="nom" class="form-control" value="{{ old('nom') ?? $user->nom }}">
                                                </div>
                                                <div class="form-group col-md-6">
                                                  <label for="prenom">Prénom</label>
                                                  <input type="text" name="prenom" id="prenom" class="form-control" value="{{ old('prenom') ?? $user->prenom }}">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-12">
                                                  <label for="email">Email</label>
                                                  <input type="email" name="email" id="email" class="form-control" value="{{ old('email') ?? $user->email }}">
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary btn-rounded">
                                                    <i class="fa fa-save mr-2"></i>
                                                    <span>Enregistrer</span>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if (auth()->user()->id === $user->id)
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Changer mon mot de passe</h5>
                        <form action="{{ route('admin.users.update-password', $user) }}" method="POST">
                            @csrf @method('PUT')
                            <div class="row">
                                <div class="form-group col-12">
                                <label for="current_password">Actuel mot de passe</label>
                                <input type="password" name="current_password" id="current_password" class="form-control @error('current_password') is-invalid @enderror" >
                                @error('current_password')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                                </div>
                                <div class="form-group col-md-12">
                                <label for="new_password">Nouveau mot de passe</label>
                                <input type="password" name="new_password" id="new_password" class="form-control @error('new_password') is-invalid @enderror">
                                @error('new_password')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-12">
                                <label for="new_password">Confirmez le nouveau mot de passe</label>
                                <input type="password" name="new_password_confirmation" id="new_password" class="form-control" >
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary btn-rounded">
                                    <i class="fa fa-save mr-2"></i>
                                    <span>Enregistrer</span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>    
        @endif
    </div>
@endsection