@extends('admin.layouts.app')

@section('content')

@section('style')
    <link rel="stylesheet" href="{{ asset('css/bootstrap-toggle.min.css') }}">
@endsection

<div class="card">
    <div class="card-header d-sm-flex d-block">
        <div class="mr-auto mb-sm-0 mb-3">
            <h4 class="card-title mb-2">Liste des utilisateurs</h4>
        </div>
        {{-- <a href="javascript:void(0);" class="btn btn-info light mr-3"><i class="las la-download scale3 mr-2"></i>Import Csv</a> --}}
        <button type="button" class="btn btn-info btn-rounded" data-toggle="modal" data-target="#user_modal_add">
            <i class="fa fa-plus-circle mr-2"></i>
            <span>Ajouter un utilisateur</span>
        </button>
        @include('User::create')
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table style-1" id="ListDatatableView">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nom</th>
                        <th>Prénom</th>
                        <th>Email</th>
                        <th>Pseudo</th>
                        <th>Status</th>
                        <th>ACTION</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $user->nom }}</td>
                        <td> {{ $user->prenom }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->pseudo }}</td>
                        {{-- <td>
                            @if ($user->status)
                                <span class="badge badge-success">ACTIF</span>
                            @else
                                <span class="badge badge-danger">SUSPENDU</span>
                            @endif
                        </td> --}}
                        <td>
                            <input data-id="{{ $user->id }}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="ACTIF" data-off="SUSPENDU" {{ $user->status ? 'checked' : '' }}>
                        </td>
                        <td>
                            <div class="d-flex action-button">
                                <button type="button" class="btn btn-info btn-xs light px-2 btn-rounded" data-toggle="modal" data-target="#user_modal_edit-{{ $user->id }}">
                                    <i class="fa fa-pencil fa-2x"></i>
                                </button>
                                @include('User::edit')
                                <a href="{{ route('admin.users.show', $user) }}" class="ml-2 btn btn-xs px-2 light btn-success">
                                    <i class="fa fa-user fa-2x"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
@section('script')
    <script src="{{ asset('js/bootstrap-toggle.min.js') }}"></script>
    <script>
    $(function (){
        $('.toggle-class').change(function () {
            var status = $(this).prop('checked') == true ? 1 : 0;
            var user_id = $(this).data('id');

            $.ajax({
                type: "GET",
                dataType: "json",
                url: "/users-change-status",
                data: {'status': status, 'user_id': user_id},
                success: function (data) {
                    console.log(data.success)
                }
            });
        })
    })
    </script>
@endsection