<?php

use App\Admin\Categorie\CategorieController;
use App\Models\Categorie;

Route::resource("categories", CategorieController::class);

// Activer/Suspendre
Route::get('categories/change-status', [CategorieController::class, 'changeStatus'])->name('categories.changeStatus');