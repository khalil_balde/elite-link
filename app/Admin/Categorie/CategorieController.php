<?php

namespace App\Admin\Categorie;

use App\Http\Controllers\Controller;
use App\Models\Categorie;
use Illuminate\Http\Request;

class CategorieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Categorie::latest()->get();
        return view('Categorie::list', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Categorie::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Categorie::create([
            'reference' => $request->reference,
            'description' => $request->description,
        ]);

        toastr()->info('La categorie a été ajoutée avec succès !!!');
        return to_route('admin.categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categorie = Categorie::find($id);
        if ($categorie) {
            return view('Categorie::details', compact('categorie'));
        } else {
            toastr()->error('La categorie selectionnée n\'existe pas !');
            return back();
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categorie = Categorie::find($id);
        if ($categorie) {
            return view('Categorie::edit', compact('categorie'));
        } else {
            toastr()->error('La categorie selectionnée n\'existe pas !');
            return back();
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $categorie = Categorie::find($id);
        if ($categorie) {
            $categorie->update([
                'reference' => $request->reference,
                'description' => $request->description,
            ]);

            toastr()->info('La categorie a été modifiée avec succès !!!');
            return to_route('admin.categories.index');
        } else {
            toastr()->error('La categorie selectionnée n\'existe pas !');
            return back();
        }
    }

    public function changeStatus(Request $request)
    {
        $categorie = Categorie::find($request->categorie_id);
        if ($categorie) {
            $categorie->status = $request->status;
            $categorie->save();

            toastr()->success('Le status de la categorie a été mise à jour !');
            return back();
        } else {
            toastr()->error('La categorie selectionnée n\'existe pas !');
            return back();
        }
    }

    // public function activer(Categorie $categorie)
    // {
    //     if ($categorie) {
    //         $categorie->status = 1;
    //         $categorie->save();
    //         toastr()->info('La categorie a été activée avec succès !!!');
    //         return to_route('admin.categories.index');
    //     } else {
    //         toastr()->error('La categorie selectionnée n\'existe pas !');
    //         return back();
    //     }  
    // }

    // public function suspendre(Categorie $categorie)
    // {
    //     if ($categorie) {
    //         $categorie->status = 0;
    //         $categorie->save();
    //         toastr()->info('La categorie a été suspendue avec succès !!!');
    //         return to_route('admin.categories.index');
    //     } else {
    //         toastr()->error('La categorie selectionnée n\'existe pas !');
    //         return back();
    //     }  
    // }
}
