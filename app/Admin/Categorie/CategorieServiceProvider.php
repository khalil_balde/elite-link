<?php

namespace App\Admin\Categorie;

use Illuminate\Support\ServiceProvider;

class CategorieServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsfrom(__DIR__.'/views', 'Categorie');
    }
}
