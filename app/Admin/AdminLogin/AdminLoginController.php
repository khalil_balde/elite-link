<?php

namespace App\Admin\AdminLogin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminLoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'store');   
    }

    public function index(){

        return view('AdminLogin::index');
    }

    public function store(Request $request)
    {
        $credentials = $request->only('pseudo', 'password');

        if (auth()->attempt(array_merge($credentials, ['status' => 1]))) {
            toastr()->success('Athentification reussie. BIENVENUE !');
            return to_route('admin.dashboard');
        } else {
            return back()->with('error', 'Echec d\'authentification, ERREUR !');
        }


    }
    
    public function logout(Request $request)
    {
        auth()->logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        toastr()->success('Deconnexion reussie, AU REVOIR !');
        return to_route('admin.login');
    }
}
