<?php

use App\Admin\AdminLogin\AdminLoginController;

Route::get('admin/login', [AdminLoginController::class, 'index'])->name('admin.login');
Route::post('admin/login', [AdminLoginController::class, 'store']);
Route::post('admin/logout', [AdminLoginController::class, 'logout'])->name('admin.logout');
