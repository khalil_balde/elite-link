<?php

use App\Admin\Caisse\CaisseController;

Route::get('caisse', [CaisseController::class, 'index'])->name('caisse.index');