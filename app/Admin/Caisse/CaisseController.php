<?php

namespace App\Admin\Caisse;

use App\Http\Controllers\Controller;
use App\Models\Caisse;
use Illuminate\Http\Request;

class CaisseController extends Controller
{
    public function index()
    {
        $soldeCaisse = Caisse::pluck('solde');
        return view('Caisse::index', compact('soldeCaisse'));
    }
}
