<?php

namespace App\Admin\Faq;

use App\Http\Controllers\Controller;
use App\Models\Faq;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $faqs = Faq::latest()->get();
        return view('Faq::index', compact('faqs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Faq::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
           Faq::create([
            'reference' => $request->reference,
            'description' => $request->description,
        ]);

        toastr()->info('La Question a été ajoutée avec succès !!!');
        return to_route('admin.faqs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $faq = Faq::find($id);
        if($faq){
            return view('Faq::details', compact('faq'));
        }
        else{
             toastr()->error('La Question selectionnée n\'existe pas !');
            return back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $faq = Faq::find($id);
        if ($faq) {
            return view('Faq::edit', compact('faq'));
        } else {
            toastr()->error('La question selectionnée n\'existe pas !');
            return back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $faq = Faq::find($id);
        if ($faq) {
            $faq->update([
                'reference' => $request->reference,
                'description' => $request->description,
            ]);

            toastr()->info('La question a été modifiée avec succès !!!');
            return to_route('admin.faqs.index');
        } else {
            toastr()->error('La question selectionnée n\'existe pas !');
            return back();
        }
    }

    public function changeStatus(Request $request)
    {
        $faq = Faq::find($request->faq_id);
        if ($faq) {
            $faq->status = $request->status;
            $faq->save();

            toastr()->success('Le status de la questin a été mise à jour !');
            return back();
        } else {
            toastr()->error('La question selectionnée n\'existe pas !');
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
