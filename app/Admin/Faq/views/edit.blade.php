<!-- Modal -->
<div class="modal fade" id="edit_categorie_modal-{{ $faq->id }}">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modification d'une categorie</h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                </button>
            </div>
            <form action="{{ route('admin.faqs.update', $faq) }}" method="POST">
                @csrf @method('PUT')
                <div class="modal-body">
                    <div class="form-group mb-4">
                      <label for="reference"><strong>Reference</strong></label>
                      <input type="text" name="reference" id="reference" class="form-control form-control-lg" value="{{ old('reference') ?? $faq->reference }}" required>
                    </div>
                    <div class="form-group">
                      <label for="description"><strong>Description</strong></label>
                      <textarea class="form-control" name="description" id="description" rows="3">{{ old('description') ?? $faq->description }}</textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded light" data-dismiss="modal">Fermer</button>
                    <button type="submit" class="btn btn-info btn-rounded">
                        <i class="fa fa-save mr-2"></i>
                        Enregistrer
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>