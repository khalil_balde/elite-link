@extends('admin.layouts.app')

@section('style')
    <link href="{{ asset('vendor/datatables/css/jquery.dataTables.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-toggle.min.css') }}">
@endsection

@section('content')

<div class="col-12">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Fees Collection</h4>
            <button type="button" class="btn btn-primary btn-rounded mb-2" data-toggle="modal" data-target="#create_categorie_modal">
                <i class="fa fa-plus-circle mr-2"></i>
                <span>Ajouter une FAQ</span>
            </button>
            @include('Faq::create')
        </div>
        <div class="card-body">
            <div class="">
                <table id="example4" class="display">
                    <thead>
                        <tr>
                            <th style="font-weight: bold">#</th>
                            <th style="font-weight: bold">Reference</th>
                            <th style="font-weight: bold">Description</th>
                            <th style="font-weight: bold">Statut</th>
                            <th style="font-weight: bold">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($faqs as $faq)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $faq->reference }}</td>
                                <td>{{ Str::limit($faq->description, 50, '...') }}</td>
                                <td>
                                    <input type="checkbox" data-id="{{ $faq->id }}" class="toggle-class" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="ACFTIF" data-off="SUSPENDU" {{ $faq->status ? 'checked' : '' }}>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-info btn-rounded btn-sm mb-2" data-toggle="modal" data-target="#edit_categorie_modal-{{ $faq->id }}">
                                        <i class="fa fa-edit mr-2"></i>
                                        <span>Editer</span>
                                    </button>
                                    @include('Faq::edit')
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
    <script src="{{ asset('vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins-init/datatables.init.js') }}"></script>
    <script src="{{ asset('js/bootstrap-toggle.min.js') }}"></script>
    <script>
        $(function () {
            $('.toggle-class').change(function () {
                 var status = $(this).prop('checked') == true ? 1 : 0;
                 var faq_id = $(this).data('id');
                 $.ajax({
                     type: 'GET',
                     dataType: 'JSON',
                     url: '/faqs-change-status',
                     data: {'status': status, 'cfaq_id': cfaq_id},
                     success: function (data) {
                        console.log('Success');
                     }
                 });
            });
        });
    </script>
@endsection