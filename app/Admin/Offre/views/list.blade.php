@extends('admin.layouts.app')

@section('style')
    <link href="{{ asset('vendor/datatables/css/jquery.dataTables.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-toggle.min.css') }}">
@endsection

@section('content')

<div class="col-12">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Listes des Offres</h4>
        </div>
        <div class="card-body">
            <ul class="list-group list-group-flush">
                @foreach ($offres as $offre)
                <div class="card border-2 border-rounded shadow-lg mb-5">
                    <div class="card-body">
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="card-header-pills mb-3">
                                    <img src="{{ asset('images/card/2.png') }}" class="w-100">
                                </div>
                            </div>
                            <div class="col-md-6 ml-2">
                                <div class="d-flex justify-content-between items-center mt-3">
                                    <h4>
                                        <span class="badge badge-rounded badge-success text-uppercase">{{ $offre->publication->titre }}</span>
                                    </h4>
                                    <h4 class="text-black ml-2">
                                        @if ($offre->status === 1)
                                            <span class="badge badge-rounded badge-success">
                                                Validée
                                            </span>
                                        @elseif ($offre->status === 0)
                                            <span class="badge badge-rounded badge-warning">
                                                En attente
                                            </span>
                                        @else
                                            <span class="badge badge-rounded badge-danger">
                                                Annulée
                                            </span>
                                        @endif
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <p class="text-justify">
                            {{ $offre->description }}
                        </p>
                        <div class="d-flex justify-content-between items-center">
                            <div>
                                <h5 style="font-size: 14px; font-weight: bolder" class="badge badge-rounded badge-outline-info p-2">
                                    <strong>{{ $offre->client->prenom . ' ' . $offre->client->nom . ', ' . ($offre->client->telephone) }}</strong><br>
                                </h5>
                            </div>
                            <div>
                                <h6 style="font-size: 14px; font-weight: bolder;">
                                    {{ $offre->created_at->translatedFormat('l jS F Y à H:i') }}
                                </h6>
                            </div>
                        </div>
                    </li> 
                    </div>
                </div>
                @endforeach
            </ul>
            <div class="mt-3">
                {{ $offres->links('pagination::bootstrap-5') }}
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
    <script src="{{ asset('vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins-init/datatables.init.js') }}"></script>

@endsection