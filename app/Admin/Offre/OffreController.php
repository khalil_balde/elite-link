<?php

namespace App\Admin\Offre;

use App\Http\Controllers\Controller;
use App\Models\Offre;

class OffreController extends Controller
{
    public function index()
    {
        $offres = Offre::latest()->paginate(10);
        return view('Offre::list', compact('offres'));
    }
}
