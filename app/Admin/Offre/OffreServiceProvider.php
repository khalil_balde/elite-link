<?php

namespace App\Admin\Offre;

use Illuminate\Support\ServiceProvider;

class OffreServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsfrom(__DIR__.'/views', 'Offre');
    }
}
