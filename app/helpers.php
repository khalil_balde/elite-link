<?php

if(! function_exists('page_title')){
    function page_title($title){
        $base_title = config('app.name');

        if( $title === '' ){
            return $base_title;    
        }
        else{
            return ' | '.$title;
        }

    }
}

if(! function_exists('funtion_categories')){
    function funtion_categories($categorie){
        $cat_null='n\'est pas une categorie';

        if($categorie === ''){
            return $cat_null; 
        }else{
            return $categorie;
        }
    }
}