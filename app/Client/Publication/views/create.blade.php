@extends('client.layouts.app')

@section('client.style')
<link href="{{ asset('assets/vendor/ckeditor5/sample/css/sample.css') }}" rel="stylesheet">
@endsection

@section('client.content')
<div class="wrapper pt-0 mt-4">
    <div class="content-post-job">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h3 class="cate-upload-title">Formulaire de publication</h3>
                    <form action="{{ route('client.publications.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="main-form">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form_group mt-30">
                                        <label class="label25">Titre</label>
                                        <input class="form_input_1 @error ('titre') is-invalid @enderror" type="text"
                                            name="titre" value="{{ old('titre') }}"
                                            placeholder="Mettez un titre de votre post ...">
                                        @error('titre')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form_group mt-30">
                                        <label class="label25">Intervalle de prix</label>
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <div class="loc_group">
                                                    <input class="form_input_1 @error ('prix_min') is-invalid @enderror"
                                                        type="number" placeholder="Prix min ..." name="prix_min"
                                                        value="{{ old('prix_min') }}">
                                                    <span class="slry-dt">GNF</span>
                                                    @error('prix_min')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <div class="loc_group">
                                                    <input class="form_input_1 @error ('prix_max') is-invalid @enderror"
                                                        type="number" placeholder="Prix max ..." name="prix_max"
                                                        value="{{ old('prix_max') }}">
                                                    <span class="slry-dt">GNF</span>
                                                    @error('prix_max')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form_group mt-30">
                                        <label class="label25">Lieu</label>
                                        <input class="form_input_1 @error ('lieu') is-invalid @enderror" type="text"
                                            name="lieu" value="{{ old('lieu') }}"
                                            placeholder="Mettez le lieu où aura le travail ...">
                                        @error('lieu')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="form_group mt-30">
                                        <label class="label25">Date d'intervention</label>
                                        <input class="form_input_1 @error ('date_intervention') is-invalid @enderror"
                                            type="date" name="date_intervention"
                                            value="{{  old('date_intervention') ?? now()->format('Y-m-d') }}"
                                            placeholder="Indiquez la date à laquelle le travail sera fait ...">
                                        @error('date_intervention')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-lg-5">
                                    <div class="form_group mt-30">
                                        <label class="label25">Categorie du Job</label>
                                        <select
                                            class="form-select form-select-lg @error ('categorie_id') is-invalid @enderror"
                                            name="categorie_id">
                                            <option selected disabled>Choisissez une catégorie ...</option>
                                            @foreach ($categories as $cat)
                                            <option value="{{ $cat->id }}">{{ $cat->reference }}</option>
                                            @endforeach
                                        </select>
                                        @error('categorie_id')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form_group mt-30">
                                        <label class="label25">Description</label>
                                        <textarea name="description"
                                            class="form-control form-control-lg @error ('description') is-invalid @enderror"
                                            rows="5"
                                            placeholder="Décrivez votre publication afin de permettre aux ouvriers de postuler leurs offres sans soucis de compréhension ...">{{ old('description') }}</textarea>
                                        @error('description')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form_group mt-30">
                                        <label class="label25">Description</label>
                                        <input type="file" name="photo"
                                            class="form-control custom-input-file @error ('photo') is-invalid @enderror"
                                            name="'photo">
                                        @error('photo')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="submit_btn">
                                        <button class="main-btn color btn-hover" type="submit">
                                            <i class="fa fa-check-square me-2"></i>
                                            <span>Publier</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-4">
                    <div class="post-job-right">
                        <img src="{{ asset('assets/images/icon-1.svg') }}" alt="">
                        <h4>Soyez precis le plus que possible.</h4>
                        <p>
                            Mettez plus de détails pour faciliter aux differents ouvriers de soumettre leur offre pour
                            effectuer votre tâche. <br>br
                            Prennez une photo claire et nette de votre truc à realiser pour donner une idée générale.
                            <br><br>
                            Prix min et prix max détermine un intervalle de discussion entre vous et votre travailleur.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection