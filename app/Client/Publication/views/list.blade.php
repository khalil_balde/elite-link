@extends('client.layouts.app')

@section('client.content')
<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="pl_item_search rrmt-30">
                    <form action="#" method="post">
                        <div class="row">
                            <div class="col-lg-8 col-md-8">
                                <div class="form_group">
                                    <input class="form_input_1" type="text"
                                        placeholder="Rechercher dans la liste des tâches ...">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-4">
                                <button class="post-link-btn color btn-hover w-100 rmt-10">
                                    <i class="fa fa-search mr-2"></i>
                                    <span>Rechercher</span>
                                </button>
                            </div>
                            <div class="col-lg-2 col-md-4">
                                <a href="{{ route('client.publications.create') }}"
                                    class="post-link-btn color btn-hover w-100 rmt-10">
                                    <i class="fa fa-plus-circle mr-2"></i>
                                    <span>Ajouter un post</span>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="all-items">
                    <div class="product-items-list">
                        <div class="row">
                            @foreach($publications as $publication)
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="full-width mt-30">
                                    <div class="recent-items">
                                        <div class="posts-list">
                                            <div class="feed-shared-product-dt">
                                                <div class="pdct-img">
                                                    <a href="{{ route('client.publications.show', $publication) }}"><img
                                                            class="ft-plus-square product-bg-w bg-cyan me-0"
                                                            src="{{ asset('storage/' . $publication->photo) }}"
                                                            alt=""></a>
                                                </div>
                                                <div class="author-dts pp-20">
                                                    <p class="job-heading pp-title">
                                                        {{ $publication->titre }}
                                                    </p>
                                                    <p class="notification-text font-small-4">
                                                        <span>{{ $publication->categorie->reference }}</span>
                                                    </p>
                                                    <div class="ppdt-price-sales">
                                                        <div class="ppdt-price">
                                                            <small>
                                                                {{ $publication->prix_min }}
                                                                GNF - {{ $publication->prix_max }} GNF
                                                            </small>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="post-meta">
                                            <div class="job-actions">
                                                <div class="aplcnts_15">
                                                    <a href="{{ route('client.publications.show', $publication->slug) }}"
                                                        class="view-btn btn-hover">Détails</a>
                                                </div>

                                                <div class="ppdt-sales view-btn">
                                                    <span>{{ $publication->vues }}</span>
                                                </div>
                                                <div class="action-btns-job">
                                                    <a href="#" class="crt-btn crt-btn-hover me-2"><i
                                                            class="fas fa-shopping-cart"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection