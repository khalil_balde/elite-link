@extends('client.layouts.app')

@section('client.content')
<div class="wrapper pt-0">
    <div class="breadcrumb-pt breadcrumb-bg mb-3">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-title">
                        <h1 class="title_text">
                            {{ $publication->titre }}
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- <div class="page-tabs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" href="product_detail_view.html">Item Details</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="product_detail_view_reviews.html">Reviews</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="product_detail_view_comments.html">Comments</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="product_detail_view_support.html">Support</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div> --}}
    <div class="event-content-main">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12">
                    <div class="prdct_dt_view">
                        {{-- <div class="product-main-info">
                            <div class="product-left-info">
                                <p class="product-label-exclusive side-indent"></p>
                            </div>
                            <div class="product-right-info">
                                <p class="product-sales-info">
                                    Sales: <span class="product-rating-and-sales-amount">402</span>
                                </p>
                                <p class="product-id">
                                    ID: <span class="product-rating-and-sales-amount">136120</span>
                                </p>
                            </div>
                        </div> --}}
                        <div class="pdct-img">
                            <img class="ft-plus-square product-bg-w bg-cyan me-0"
                                src="{{ asset('storage/'. $publication->photo) }}" alt="">
                        </div>
                        <div class="py-5 px-3">
                            <div class="d-flex justify-content-between items-center">
                                <div>
                                    <i class="fa fa-tag me-2"></i>
                                    <span>{{ $publication->categorie->reference }}</span>
                                </div>
                                <div>
                                    <i class="fa fa-map-marker-alt me-2"></i>
                                    <span>{{ $publication->lieu }}</span>
                                </div>
                                <div>
                                    <i class="fa fa-calendar-alt me-2"></i>
                                    <span>{{ $publication->date_intervention }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="full-width mt-30">
                        <div class="item-description">
                            <div class="jobtxt47">
                                <h4>Description</h4>
                                <p style="text-align: justify">
                                    {{ $publication->description }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="event-card rmt-30">
                        <div class="product_license_dts">
                            <ul class="evnt_cogs_list">
                                <li>
                                    <div class="product_license_check">
                                        <div class="radio-btn enstng-btn">
                                            <label for="eventsetting1" class="evsettng_text">
                                                Prix minimal
                                            </label>
                                        </div>
                                        <span class="item_price">{{ $publication->prix_min }} GNF</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="product_license_check">
                                        <div class="radio-btn enstng-btn">
                                            <label for="eventsetting2" class="evsettng_text">
                                                Prix maximal
                                            </label>
                                        </div>
                                        <span class="item_price">{{ $publication->prix_max }} GNF</span>
                                    </div>
                                </li>
                            </ul>
                            @if ($publication->status === 0)
                            <div class="item_buttons">
                                <div class="purchase_form_btn">
                                    <form action="{{ route('client.offres.store') }}" method="post">
                                        @csrf

                                        <div class="main-form">
                                            <div class="row border-2 shadow-lg py-3">
                                                <h3
                                                    class="cate-upload-title mt-3 text-center text-primary text-uppercase">
                                                    Soumettre une offre</h3>
                                                <input type="hidden" name="slug"
                                                    value="{{ $publication->slug }}">
                                                <div class="col-lg-12">
                                                    <div class="form_group mt-30">
                                                        <label class="label25">Description</label>
                                                        <textarea name="description"
                                                            class="form-control form-control-lg @error ('description') is-invalid @enderror"
                                                            rows="5"
                                                            placeholder="Décrivez votre offre afin de permettre au client de la comprendre et surtout d'optimiser votre chance d'être pris ...">{{ old('description') }}</textarea>
                                                        @error('description')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="col-lg-12">
                                                    <div class="form_group mt-30">
                                                        <label class="label25">Prix de l'offre</label>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="loc_group">
                                                                    <input
                                                                        class="form_input_1 @error ('prix') is-invalid @enderror"
                                                                        type="number"
                                                                        placeholder="Un prix entre {{ $publication->prix_min . ' GNF et ' . $publication->prix_max . ' GNF' }} ..."
                                                                        name="prix" value="{{ old('prix') }}">
                                                                    <span class="slry-dt">GNF</span>
                                                                    @error('prix')
                                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-12">
                                                    <div class="submit_btn">
                                                        <button class="main-btn color btn-hover" type="submit">
                                                            <i class="fa fa-check-square me-2"></i>
                                                            <span>valider</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            @endif
                            <div class="purchased_cards">
                                <img src="{{ asset('assets/images/paypal.png') }}" alt="">
                                <img src="{{ asset('assets/images/mastercard.png') }}" alt="">
                                <img src="{{ asset('assets/images/visa.png') }}" alt="">
                                <img src="{{ asset('assets/images/discover.png') }}" alt="">
                                <img src="{{ asset('assets/images/americanexpress.png') }}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="event-card mt-30">
                        <div class="product_total_stats">
                            <strong class="product_total_numberic">
                                <span class="product_stats_icon">
                                    <i class="fas fa-eye"></i>
                                </span>
                                {{ $publication->vues }}
                            </strong>
                        </div>
                        <div class="product_total_stats">
                            <strong class="product_total_numberic">
                                <span class="product_stats_icon">
                                    <i class="fas fa-eye"></i>
                                </span>
                                {{ $publication->offresCount }}
                            </strong>
                        </div>
                    </div>
                    <div class="full-width mt-30">
                        <div class="headtte14m">
                            <h4>Autres informations</h4>
                        </div>
                        <div class="product-description-sidebar">
                            <div class="product-features-block">
                                <p class="product-features-title">Publié le</p>
                                <span class="product-update-time">
                                    <h6>{{ $publication->created_at->format('d-m-Y à H:i') }}</h6>
                                </span>
                                <p class="product-features-title">Statut</p>
                                <span class="product-features-tags text-uppercase">
                                    @if ($publication->status === -1)
                                    <h4>Annulée</h4>
                                    @elseif ($publication->status === 0)
                                    <h4>En attente</h4>
                                    @else
                                    <h4>Validée</h4>
                                    @endif
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if (auth('front')->id() === $publication->client->id)

            <div class="wrapper pt-0">
                <div class="refund-main-wrapper mt-5">
                    <div class="container">
                        <div class="row card p-5 mt-4">
                            <div class="col-12">
                                <h3>
                                    Liste des offres soumises
                                </h3>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    <div class="main-table mt-30">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead class="thead-dark">
                                                    <tr>
                                                        <th scope="col">#</th>
                                                        <th scope="col">Date</th>
                                                        <th scope="col">Soumettant</th>
                                                        <th scope="col">Prix (GNF)</th>
                                                        <th scope="col">Statut</th>
                                                        <th scope="col">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($publication->offres as $offre)
                                                    <tr>
                                                        <td>{{ $loop->iteration }}</td>
                                                        <td>{{ $offre->created_at->format('d-m-Y à H:i') }}</td>
                                                        <td>
                                                            {{ $offre->client->fullName() }}
                                                        </td>
                                                        <td>{{ $offre->prix }}</td>
                                                        <td>
                                                            @if ($offre->status === -1)
                                                            <span class="label-dker color_refund_open">Annulée</span>
                                                            @elseif ($offre->status === 0)
                                                            <span class="label-dker color-fmt">En attente</span>
                                                            @else
                                                            <span class="label-dker color_bb text-light">Validée</span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <a href="{{ route('client.offres.show', $offre) }}"
                                                                class="ref-inv-btn">Détails</a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection