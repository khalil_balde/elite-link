<?php

namespace App\Client\Publication;

use App\Models\Publication;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Categorie;
use App\Notifications\PublicationNotification;
use Illuminate\Support\Facades\Notification;

class PublicationController extends Controller
{
    public function __construct() {
        $this->middleware('auth:front')->except('index', 'show');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $publications = Publication::with('client', 'categorie')->latest()->get();
        return view('PublicationClient::list', compact('publications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Categorie::query()->latest()->get();
        return view('PublicationClient::create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'photo' => 'required|image|mimes:png,jpg,jpeg,gif,svg|max:5000',
            'titre' => 'required|min:10',
            'lieu' => 'required',
            'categorie_id' => 'required',
            'prix_max' => 'required',
            'prix_min' => 'required',
            'date_intervention' => 'required|date',
            'description' => 'required|min:50',
        ]);

        $photo = $request->photo->store('publications');
        $titre = $request->titre;
        $slug = Str::slug($titre) . '_' . now()->format('d-m-Y_His');

        $pub = Publication::create([
            'photo' => $photo,
            'titre' => $titre,
            'slug' => $slug,
            'vues' => 5 ,
            'lieu' => $request->lieu,
            'categorie_id' => $request->categorie_id,
            'client_id' => auth('front')->id(),
            'prix_max' => $request->prix_max,
            'prix_min' => $request->prix_min,
            'date_intervention' => $request->date_intervention,
            'description' => $request->description,
        ]);

        // Notification::send(auth('front')->user(), new PublicationNotification($pub));

        return to_route('client.publications.index');
        toastr()->info('La publication a été crée avec succès !!!');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Publication  $publication
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $publication = Publication::with('client', 'offres', 'offres')
                            ->where('slug', $slug)->first();
        
        if ($publication) {
            return view('PublicationClient::details', compact('publication'));
        } else {
            toastr()->error('La publication selectionnée n\'existe pas !');
            return to_route('client.publications.index');
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Publication  $publication
     * @return \Illuminate\Http\Response
     */
    public function edit(Publication $publication)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Publication  $publication
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Publication $publication)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Publication  $publication
     * @return \Illuminate\Http\Response
     */
    public function destroy(Publication $publication)
    {
        //
    }
}
