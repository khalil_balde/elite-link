<?php

use App\Client\Publication\PublicationController;

Route::resource('publications', PublicationController::class)->parameters([
    'publications' => 'slug'
]);

// Route::controller('publications', PublicationController::class, )->name('publications.');