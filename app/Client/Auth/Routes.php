<?php
use App\Client\Auth\AuthClientContsroller;
//route login
Route::get('login', [AuthClientContsroller::class, 'index'])->name('login');
Route::post('login', [AuthClientContsroller::class, 'login']);
Route::post('logout', [AuthClientContsroller::class, 'logout'])->name('logout');

//route register
Route::get('register', [AuthClientContsroller::class, 'registerIndex'])
        ->name('register');
Route::post('register', [AuthClientContsroller::class, 'register']);
