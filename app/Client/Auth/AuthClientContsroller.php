<?php

namespace App\Client\Auth;

use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\Publication;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthClientContsroller extends Controller
{
    public function __construct() {
        $this->middleware('auth:front')->only('logout', 'profil');
    }

    public function index() { 
        return view('Auth::index');
    }

    public function login(Request $request) {

        $credentials = request()->only('pseudo', 'password');

        if(Auth::guard('front')->attempt(array_merge( $credentials, ['status' => 1 ] ))){

            $request->session()->regenerate();
            return to_route('client.accueil.index');

        }else{
            return back()->with('error', 'Informations fournies incorrectes ! ');
        }
        

    }

    public function logout(Request $request) {

        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        toastr()->success('Déconnexion reussie, AU REVOIR !');
        return to_route('client.accueil.index');

    }

    public function registerIndex()
    {
        return view('Auth::register');
    }

    public function register(Request $request) 
    {
        $this->validate($request, [
            'nom' => 'required',
            'prenom' => 'required',
            'email' => 'required|email',
            'telephone' => 'required',
            'adresse' => 'required',
            'pseudo' => 'required',
            'password' => 'required|confirmed',
        ]);

        $user = Client::create([
            'nom' => $request->nom,
            'prenom' => $request->prenom,
            'email' => $request->email,
            'telephone' => $request->telephone,
            'adresse' => $request->adresse,
            'pseudo' => $request->pseudo,
            'password' => Hash::make($request->password),
        ]);

        auth()->login($user);

        toastr()->success('Votre compte a été créé avec succès.');
        return to_route('client.accueil.index');

    }

    public function edit(Client $client)
    {
        if ($client) {
            return view('Auth::edit', compact('client'));
        } else {
            toastr()->error('Le compte selectionné n\'existe pas !');
            return to_route('admin.dashboard');
        }
    }


}
