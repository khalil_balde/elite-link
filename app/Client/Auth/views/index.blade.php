<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from gambolthemes.net/html-items/micko_demo/sign_in.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Apr 2022 13:18:35 GMT -->
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, shrink-to-fit=9">
    <title>{{ config('app.name') }} - Se connecter</title>

    <link rel="icon" type="image/png" href="{{ asset('assets/images/fav.png') }}">

    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet">

    <link href="{{ asset('assets/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
</head>
<body>

<div class="form-wrapper">
    <div class="app-form">
        <div class="app-form-sidebar">
            <div class="sign-logo">
                <img src="assets/images/logo_2.png" alt="">
            </div>
            <div class="sign_sidebar_text">
                <h1>
                    La solution de vos soucis à trouver des ouvriers qualifiés ou des patrons reglos. <br>
                    Exposer vos soucis pour trouver de l'aide <i class="fa fa-hands-helping"></i>
                </h1>
            </div>
        </div>
        <div class="app-form-content">
            <div class="container">
                <div class="row justify-content-center">
                <div class="col-lg-10 col-md-10">
                    <div class="app-top-items">
                        <div class="app-top-left-logo">
                            <img src="{{ asset('assets/images/logo.png') }}" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-md-8 shadow-lg p-4">
                    <div class="registration">
                        <h2 class="registration_title">Veuillez vous connecter pour beneficier de nos services ...</h2>
                        @if (session('error'))
                        <div class="text-center my-4">
                            <span class="badge badge-danger px-4 py-3" style="font-size: 18px">
                                {{ session('error') }}
                            </span>
                        </div>
                        @endif
                        <form method="post" action="{{ route('client.login') }}">
                            @csrf
                            <div class="form_group mt-30">
                                <label class="label25">Pseudo</label>
                                <input class="reg_form_input_1" name="pseudo" placeholder="" value="{{ old('pseudo') }}">
                            </div>
                            <div class="form_group mt-25">
                                <div class="field_password">
                                    <label class="label25">Mot de passe</label>
                                </div>
                                <div class="loc_group">
                                    <input class="reg_form_input_1" name="password" type="password">
                                </div>
                            </div>
                            <button class="btn-register btn-hover" type="submit">Connexion <i class="fas fa-sign-in-alt ms-2"></i></button>
                        </form>
                        <div class="card mt-4 p-3 text-center rounded">
                            <h3 class="">
                                Vous n'avez pas encore de compte ? 
                                <a class="SidebarRegister__link" href="{{ route('client.register') }}">Créez !</a>
                            </h3>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>

<!-- Mirrored from gambolthemes.net/html-items/micko_demo/sign_in.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Apr 2022 13:18:36 GMT -->
</html>