<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from gambolthemes.net/html-items/micko_demo/sign_up.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Apr 2022 13:18:36 GMT -->
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, shrink-to-fit=9">
<title>{{ config('app.name') }} - S'enregistrer</title>

<link rel="icon" type="image/png" href="{{ asset('assets/images/fav.png') }}">

<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet">

<link href="{{ asset('assets/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
</head>
<body>
    <div class="form-wrapper">
        <div class="app-form">
            <div class="app-form-sidebar">
                <div class="sign-logo">
                    <img src="{{ asset('assets/images/logo_2.png') }}" alt="">
                </div>
                <div class="sign_sidebar_text">
                    <h1>
                        La solution de vos soucis à trouver des ouvriers qualifiés ou des patrons reglos. <br>
                        Exposer vos soucis pour trouver de l'aide <i class="fa fa-hands-helping"></i>
                    </h1>
                </div>
            </div>
            <div class="app-form-content">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-10 card p-4 shadow-lg">
                            <div class="content-post-job">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-1é">
                                            <h3 class="cate-upload-title fw-bolder"> Veuillez entrer vos informations ...</h3>
                                            <div class="main-form">
                                                <form action="{{ route('client.register') }}" method="post">
                                                    @csrf
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form_group mt-30">
                                                                <label class="label25 fw-bolder">Nom</label>
                                                                <input class="form_input_1" type="text" name="nom" class="form-control form-control-lg fw-bolder @error('nom') is-invalid @enderror" value="{{ old('nom') }}">
                                                                @error('nom')
                                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form_group mt-30">
                                                                <label class="label25 fw-bolder">Prénom</label>
                                                                <input class="form_input_1" type="text" name="prenom" class="form-control form-control-lg fw-bolder @error('prenom') is-invalid @enderror" value="{{ old('prenom') }}">
                                                                @error('prenom')
                                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form_group mt-30">
                                                                <label class="label25 fw-bolder">Email</label>
                                                                <input class="form_input_1" type="email" name="email" class="form-control form-control-lg fw-bolder @error('email') is-invalid @enderror" value="{{ old('email') }}">
                                                                @error('email')
                                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form_group mt-30">
                                                                <label class="label25 fw-bolder">Telephone</label>
                                                                <input class="form_input_1" type="text" name="telephone" class="form-control form-control-lg fw-bolder @error('telephone') is-invalid @enderror" value="{{ old('telephone') }}">
                                                                @error('telephone')
                                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form_group mt-30">
                                                                <label class="label25 fw-bolder">Address</label>
                                                                <input class="form_input_1" type="text" name="adresse" class="form-control form-control-lg fw-bolder @error('adresse') is-invalid @enderror" value="{{ old('adresse') }}">
                                                                @error('adresse')
                                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-md-6">
                                                            <div class="form_group mt-30">
                                                                <label class="label25 fw-bolder">Pseudo</label>
                                                                <input class="form_input_1" type="text" name="pseudo" class="form-control form-control-lg fw-bolder @error('pseudo') is-invalid @enderror" value="{{ old('pseudo') }}">
                                                                @error('pseudo')
                                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form_group mt-30">
                                                                <label class="label25 fw-bolder">Mot de passe</label>
                                                                <input class="form_input_1" type="password" name="password" class="form-control form-control-lg fw-bolder @error('password') is-invalid @enderror" value="{{ old('password') }}">
                                                                @error('password')
                                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form_group mt-30">
                                                                <label class="label25 fw-bolder">Confirmez votre mot de passe</label>
                                                                <input class="form_input_1" type="password" name="password_confirmation" class="form-control form-control-lg fw-bolder ">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 offset-md-3">
                                                            <div class="form_group mt-30">
                                                                <button type="submit" class="btn-register btn-hover">
                                                                    <i class="fa fa-check mr-2"></i>
                                                                    <span>Valider</span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                <div class="card mt-4 p-3 text-center rounded">
                                                    <h3 class="">
                                                        Vous avez déjà un compte ? 
                                                        <a class="SidebarRegister__link" href="{{ route('client.login') }}">Connectez-vous !</a>
                                                    </h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>