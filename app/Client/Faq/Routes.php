<?php

use App\Client\Faq\FaqClientController;

Route::get('faqsClient', [FaqClientController::class, 'indexClient'])->name('client.faq');