@extends('client.layouts.app')

@section('client.content')

<div class="wrapper pt-0">
    <div class="help-hero-search pt-3">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-md-6">
                    <div class="help-hero-banner">
                        <div class="help-hero-search-greeting">
                            <h3 class="hero-search-greeting">Comment pouvons-nous vous aider ?</h3>
                            <div class="hero-search_form">
                            <input class="form_input_1" type="text" placeholder="Votre Question ?" value="">
                            <button class="search-hero-btn" type="submit"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="refund-main-wrapper mt-5">
        <div class="container">
            <div class="row">

                <div class="col-xl-12 col-lg-8 col-md-12">
                    
                    <div class="help-suggest-links mt-25">
                        <h4 class="help-title"><i class="far fa-life-ring me-3"></i>Des Repons qui pourront vous aider</h4>
                        <div class="suggest-links-list">

                        @foreach ($faqs as $faq)

                            <div class="suggest-link-item">
                                <a href="#" class="suggest-link">{{ $faq->reference }}</a>
                                <p class="suggest-link_description mb-0">
                                {{ $faq->description }}
                                </p>
                            </div>
                            
                        @endforeach
                            
                        </div>
                    </div>
                    <div class="helpform">
                        <svg id="bold" enable-background="new 0 0 32 32" height="60" viewBox="0 0 32 32" width="60" xmlns="http://www.w3.org/2000/svg"><path d="m26 32h-20c-3.314 0-6-2.686-6-6v-20c0-3.314 2.686-6 6-6h20c3.314 0 6 2.686 6 6v20c0 3.314-2.686 6-6 6z" fill="#f5e6fe" /><path d="m21.5 8h-11c-.273 0-.5.227-.5.5v5.667c0 1.013.82 1.833 1.833 1.833h1.673c.54-.807 1.46-1.333 2.493-1.333s1.953.527 2.493 1.333h1.673c1.015 0 1.835-.82 1.835-1.833v-5.667c0-.273-.227-.5-.5-.5zm-2.5 4.167c0 .202-.122.384-.309.462-.062.026-.127.038-.191.038-.13 0-.258-.051-.354-.146l-.524-.524-.95.871c-.16.148-.397.175-.587.067l-.835-.477-1.397 1.396c-.097.097-.225.146-.353.146s-.256-.049-.354-.146c-.195-.195-.195-.512 0-.707l1.667-1.667c.159-.16.406-.193.602-.081l.849.485.651-.596-.434-.434c-.143-.143-.186-.358-.108-.545s.258-.309.46-.309h1.667c.276 0 .5.224.5.5z" fill="#be63f9" /><g fill="#142fdb"><circle cx="10.667" cy="18" r="1.333" /><path d="m12.447 20.267c-.673.507-1.113 1.32-1.113 2.233v.167h-2.834c-.273 0-.5-.227-.5-.5v-.333c0-1.014.82-1.834 1.833-1.834h1.667c.347 0 .673.1.947.267z" /><circle cx="21.333" cy="18" r="1.333" /><path d="m24 21.833v.333c0 .273-.227.5-.5.5h-2.833v-.166c0-.913-.44-1.727-1.113-2.233.273-.167.599-.267.946-.267h1.667c1.013 0 1.833.82 1.833 1.833z" /><circle cx="16" cy="17.667" r="2" /><path d="m17.833 20.667h-3.667c-1.011 0-1.833.822-1.833 1.833v1c0 .276.224.5.5.5h6.333c.276 0 .5-.224.5-.5v-1c.001-1.011-.822-1.833-1.833-1.833z" /></g><path d="m22.833 9h-13.666c-.276 0-.5-.224-.5-.5s.224-.5.5-.5h13.667c.276 0 .5.224.5.5s-.225.5-.501.5z" fill="#d9a4fc" /></svg>
                        <h3>Forum D'aide</h3>
                        <p>Avez-vous une autre question ? Vous pouvez nous<br> contacter </p>
                        <a href="{{ route('client.client.contact') }}" class="form-link-btn btn-hover">Nous Contacter</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>




@stop



