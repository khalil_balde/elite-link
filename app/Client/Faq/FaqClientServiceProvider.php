<?php

namespace App\Client\Faq;

use Illuminate\Support\ServiceProvider;

class FaqClientServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     
     * @return void
     */
    public function boot()
    {
        $this->loadViewsfrom( __DIR__ . "/views", "Faq");
    }
}
