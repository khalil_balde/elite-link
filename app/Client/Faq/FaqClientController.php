<?php

namespace App\Client\Faq;

use App\Http\Controllers\Controller;
use App\Models\Faq;

class FaqClientController extends Controller
{
public function indexClient()
{
        $faqs = Faq::all();
        return view("Faq::index", compact('faqs'));

    }
}
