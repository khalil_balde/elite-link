<?php

namespace App\Client\User;

use App\Models\Client;
use App\Models\Publication;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $user)
    {
        if ($user) {

            $publications = Publication::with('client')->whereBelongsTo($user)->latest()->get();
            // dd($publications);
            return view('ClientUser::profil', compact('user', 'publications'));
        } else {
            toastr()->error('L\'utilisateur selectionné n\'existe pas ! ');
            return to_route('client.accueil.index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Client  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $user)
    {
        if ($user) {
            return view('ClientUser::edit', compact('user'));
        } else {
            toastr()->error('Le compte selectionné n\'existe pas !');
            return to_route('admin.dashboard');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Client  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $user)
    {
        if ($user) {
            $this->validate($request, [
                'nom' => 'required',
                'prenom' => 'required',
                'email' => 'required|email',
                'telephone' => 'required',
                'adresse' => 'required',
            ]);
    
            $user->update([
                'nom' => $request->nom,
                'prenom' => $request->prenom,
                'email' => $request->email,
                'telephone' => $request->telephone,
                'adresse' => $request->adresse,
            ]);
    
            toastr()->success('Votre compte a été mise à jour.');
            return to_route('client.users.show', compact('user'));            
        }  else {
            toastr()->error('Le compte selectionné n\'existe pas !');
            return to_route('admin.dashboard');
        }
    }

    public function updatePassword(Request $request, Client $user)
    {
        $this->validate($request, [
            'actuel_password' => 'required',
            'nouveau_password' => 'required|confirmed',
        ]);

        if (Hash::check($request->nouveau_password, $user->password)) {
            $user->password = Hash::make($request->nouveau_password);
            $user->save();

            toastr()->success('Le mot de passe a été mise à jour avec succès !');
            return back();
        } else {
            toastr()->error('Erreur de mise à jour !');
            return back();         
        }
    }

}
