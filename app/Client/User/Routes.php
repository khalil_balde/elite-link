<?php

use App\Client\User\UserController;

Route::resource('users', UserController::class)->only('show', 'edit', 'update');

Route::post('users/{user}', [UserController::class, 'updatePAssword'])
        ->name('users.update-password');