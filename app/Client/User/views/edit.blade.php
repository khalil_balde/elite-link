@extends('client.layouts.app')

@section('client.content')
<div class="wrapper">
  <div class="container">
    <div class="row">
      <div class="col-8 offset-2 col-md-10 offset-md-1">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title text-center">Editer votre profil</h3>
          </div>
          <div class="card-body">
            <div class="basic-form">
              <form action="{{ route('client.users.update', $user) }}" method="POST">
                @csrf @method('PATCH')
                <div class="row">
                  <div class="form-group col-md-6 mb-4">
                    <label class="form-label">Nom</label>
                    <input type="text" class="form-control form-control-lg" name="nom" value="{{ old('nom') ?? auth('front')->user()->nom }}">
                  </div>
                  <div class="form-group col-md-6 mb-4">
                    <label class="form-label">Prénom</label>
                    <input type="text" class="form-control form-control-lg" name="prenom" value="{{ old('prenom') ?? auth('front')->user()->prenom }}">
                  </div>
                  <div class="form-group col-md-6 mb-4">
                    <label class="form-label">Email</label>
                    <input type="email" class="form-control form-control-lg" name="email" value="{{ old('email') ?? auth('front')->user()->email }}">
                  </div>
                  <div class="form-group col-md-6 mb-4">
                    <label class="form-label">Téléphone</label>
                    <input type="text" class="form-control form-control-lg" name="telephone" value="{{ old('telephone') ?? auth('front')->user()->telephone }}">
                  </div>
                  <div class="form-group col-md-6 mb-4">
                    <label class="form-label">Adresse</label>
                    <input type="text" class="form-control form-control-lg" name="adresse" value="{{ old('adresse') ?? auth('front')->user()->adresse }}">
                  </div>
                </div>
                <div class="row mt-4">
                  <div class="col-4"></div>
                  <div class="col-4">
                    <button type="submit" class="btn btn-primary text-primary">
                      <i class="fa fa-check-circle me-2"></i>
                      <span>Enregistrer</span>
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection