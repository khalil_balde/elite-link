@extends('client.layouts.app')

@section('client.content')
<div class="wrapper pt-0">
    <div class="main-background-cover breadcrumb-pt">
        <div class="banner-user" style="background-image:url(assets/images/banners/bg-2.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="banner-item-dts">
                            <div class="banner-content">
                                <div class="banner-media">
                                    <div class="banner-media-body">
                                        <h1 style="font-size: 32px; text-transform: uppercase" class="item-user-title">
                                            {{ auth('front')->user()->fullName()}}
                                        </h1>
                                        <h3 class="item-username">{{ auth('front')->user()->pseudo }}</h3>
                                        <ul class="user-meta-btns">
                                            <li>
                                                <a href="{{ route('client.users.edit', auth('front')->user()) }}"
                                                    class="profile-edit-btn btn-hover">
                                                    <i class="fas fa-edit me-2"></i>
                                                    <span>Modifier vos infos</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="dtpgusr12 mt-2">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="pf-deferred-area">
                        <div class="pf-deferred-content">
                            <div
                                class="pf-deferred-dashboard_header d-lg-flex justify-content-between items-baseline px-4">
                                <div class="">
                                    <h4>Dashboard</h4>
                                    <p>Votre espace privé </p>
                                </div>
                                <div class="event-card rmt-30 mb-5 mb-lg-0">
                                    <div class="product_total_stats">
                                        <strong class="product_total_numberic">
                                            {{ $publications->count() }}
                                        </strong>
                                        <span class="product_stats_label">{{ Str::plural('Publication',
                                            $publications->count()) }}</span>
                                    </div>
                                </div>
                                <a href="{{ route('client.publications.create') }}"
                                    class="profile-edit-btn btn-hover mb-5 mb-lg-0">
                                    <i class="fa fa-plus-circle mr-2"></i>
                                    <span>Ajouter une publication</span>
                                </a>
                            </div>
                            <div class="pf-deferred-dashboard_card-section">
                                <div class="row text-info">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-lg-6 mb-3">
                                                <h5>
                                                    <i class="fa fa-at"></i>
                                                    <span>{{ $user->email }}</span>
                                                </h5>
                                            </div>
                                            <div class="col-lg-6 mb-3">
                                                <i class="fa fa-phone-alt me-2"></i>
                                                <span>{{ $user->telephone }}</span>
                                            </div>
                                            <div class="col-lg-6 mb-3">
                                                <i class="fa fa-user-check me-2"></i>
                                                <span>{{ $user->created_at->format('d/m/Y à H:i') }}</span>
                                            </div>
                                            <div class="col-lg-6 ">
                                                <i class="fa fa-map-marker-alt me-2"></i>
                                                <span>{{ $user->adresse }}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <form
                                            action="{{ route('client.users.update-password', auth('front')->user()) }}"
                                            method="post">
                                            @csrf
                                            <div class="row">
                                                <h4>Changez votre mot de passe</h4>
                                                <div class="form-group col-md-6 mb-3">
                                                    <label for="actuel_password" class="form-label"></label>
                                                    <input type="password" class="form-control form-control-lg @error ('actuel_password') is-invalid @enderror"
                                                        name="actuel_password" id="actuel_password">
                                                        @error('actuel_password')
                                                            <div class="invalid-feedback">{{ $message }}</div>
                                                        @enderror
                                                </div>
                                                <div class="form-group col-md-6 mb-3">
                                                    <label for="nouveau_password" class="form-label"></label>
                                                    <input type="password" class="form-control form-control-lg @error ('nouveau_password') is-invalid @enderror"
                                                        name="nouveau_password" id="nouveau_password">
                                                        @error('nouveau_password')
                                                            <div class="invalid-feedback">{{ $message }}</div>
                                                        @enderror
                                                </div>
                                                <div class="form-group col-md-6 mb-3">
                                                    <label for="nouveau_password_confirmation"
                                                        class="form-label"></label>
                                                    <input type="password" class="form-control form-control-lg"
                                                        name="nouveau_password_confirmation"
                                                        id="nouveau_password_confirmation">
                                                </div>
                                                <div class="form-group col-md-6 mb-3">
                                                    <button type="submit" class="profile-edit-btn btn-hover">
                                                        <i class="fa fa-check-circle"></i>
                                                        <span>Valider</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="event-card mt-30">
                        <div class="headtte14m">
                            <span><i class="fas fa-list-ul"></i></span>
                            <h4>Mes publications</h4>
                        </div>
                        <div class="all-activities">
                            <div class="activities-items">
                                <div class="activities-noti-acts">
                                    <div class="activities-noti-list">
                                        <ul class="list-group-horizontal-md">
                                            @foreach ($publications as $pub)
                                            <li class="list-group-item-info px-2 py-3 mb-3">
                                                <div class="d-flex justify-content-between items-center">
                                                    <h4><a href="{{ route('client.publications.show', $pub->slug) }}">{{
                                                            $pub->titre }}</a></h4>
                                                    <h4>
                                                        <span class="badge badge-danger badge-circle p-2">
                                                            {{ $pub->offres->count() . ' ' . Str::plural('offre',
                                                            $pub->offres->count()) }}
                                                        </span>
                                                    </h4>
                                                </div>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <div class="activites-pagination main-pagination">
                                    <nav aria-label="Page navigation example">
                                        {{-- <div>{{ $publications->links('pagination::bootstrap-5') }}</div> --}}
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection