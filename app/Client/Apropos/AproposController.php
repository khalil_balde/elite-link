<?php

namespace App\Client\Apropos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AproposController extends Controller
{
    public function index(){
        return view('Apropos::index');
    }
}
