<?php

use App\Client\Apropos\AproposController;

Route::get('apropos', [AproposController::class, 'index'])->name('client.apropos');