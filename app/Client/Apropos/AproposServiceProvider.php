<?php

namespace App\Client\Apropos;

use Illuminate\Support\ServiceProvider;

class AproposServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsfrom(__DIR__ . '/views', 'Apropos');
    }
}
