@extends('client.layouts.app')

@section('client.content')

<div class="wrapper pt-0">
<div class="breadcrumb-pt breadcrumb-bg">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="breadcrumb-title">
<nav aria-label="breadcrumb">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="index.html">Home</a></li>
<li class="breadcrumb-item active" aria-current="page">A propos</li>
</ol>
</nav>
<h1 class="title_text">A propos de nous</h1>
</div>
</div>
</div>
</div>
</div>

<div class="about-wrapper">

    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12">
                <div class="about-us-content">
                    <div class="item-subtitle">Nous somme</div>
                        <h2 class="item-title">We Improve Your Experience Day by Day</h2>
                        <p class="pitem-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec egestas viverra imperdiet. Vestibulum ultrices faucibus aliquet. Donec consectetur mollis dapibus. Morbi a elementum metus, vitae scelerisque eros. Fusce in est id enim imperdiet consequat eget quis orci. In accumsan enim ut suscipit pulvinar. Nam aliquam odio sit amet tellus tincidunt congue. Etiam a orci lorem. Pellentesque ultricies ligula tortor, et lobortis ipsum fringilla quis. Integer imperdiet tempor sem eu interdum. Donec finibus odio in consectetur ornare. Praesent aliquam ullamcorper metus sit amet efficitur.</p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="about-img">
                        <img class="img-about" src="assets/images/about/img-1.jpg" alt="">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="about-wrapper bg-white">
        <div class="container">
            <div class="row">
            
                <div class="col-lg-12 col-md-12">
                    <div class="about-us-content text-center process-title">
                        <div class="item-subtitle">Our Partner</div>
                        <h2 class="item-title">Micko is the multipurpose marketplace for Digital </h2>
                    </div>
                </div>
                
              <div class="owl-carousel learning_slider owl-theme">

                    <div class="col-lg-12 col-md-4 col-sm-4">
                        <div class="ach_page__badges__item">
                            <img src="assets/images/badges/hotshot.svg" alt="">
                            <h3>HotShot</h3>
                            <p>Earned more than <b>$1K</b></p>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-4 col-sm-4">
                        <div class="ach_page__badges__item">
                            <img src="assets/images/badges/enthusiast.svg" alt="">
                            <h3>Enthusiast</h3>
                            <p>Earned more than <b>$100</b></p>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-4 col-sm-4">
                        <div class="ach_page__badges__item">
                            <img src="assets/images/badges/master.svg" alt="">
                            <h3>Master</h3>
                            <p>Earned more than <b>$64k</b></p>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-4 col-sm-4">
                        <div class="ach_page__badges__item">
                            <img src="assets/images/badges/guru.svg" alt="">
                            <h3>Guru</h3>
                            <p>Earned more than <b>$100k</b></p>
                        </div>
                    </div>

              </div>
                
               
            </div>
        </div>
    </div>

    <div class="about-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="about-us-content text-center process-title mb-50">
                        <div class="item-subtitle">Notre Equipe</div>
                        <h2 class="item-title">A perfect blend of creativity and technical wizardry</h2>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="about-us-content">
                        <p class="pitem-description">Morbi eget elit eget turpis varius mollis eget vel massa. Donec porttitor, sapien eget commodo vulputate, erat felis aliquam dolor, non condimentum libero dolor vel ipsum. Sed porttitor nisi eget nulla ullamcorper eleifend. Fusce tristique sapien nisi, vel feugiat neque luctus sit amet. Quisque consequat quis turpis in mattis. Maecenas eget mollis nisl. Cras porta dapibus est, quis malesuada ex iaculis at. Vestibulum egestas tortor in urna tempor, in fermentum lectus bibendum. In leo leo, bibendum at pharetra at, tincidunt in nulla. In vel malesuada nulla, sed tincidunt neque. Phasellus at massa vel sem aliquet sodales non in magna. Ut tempus ipsum sagittis neque cursus euismod. Vivamus luctus elementum tortor, ac aliquet dolor vehicula et.</p>
                        <a href=" {{ route('client.')}} " class="main-link-btn btn-hover">Nous contacter</a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="about-img">
                        <img class="img-about" src="assets/images/about/img-2.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



@stop