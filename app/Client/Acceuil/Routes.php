<?php

use App\Client\Acceuil\AcceuilController;

Route::get('/', [AcceuilController::class, 'index'])->name('accueil.index');