@extends('client.layouts.app', ['title'=>'accueil'])

@section('client.content')



<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12">

                <div class="btn_1589">
                    <a href="{{ route('client.publications.create') }}" class="post-link-btn btn-hover">Publier une annonce</a>
                </div>

                <div class="all-items">
                    <div class="product-items-list">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="learning_all_items mt-30">
                                
                                    <div class="owl_title d-flex justify-content-between">
                                        <h4>Informatique</h4>
                                    </div>

                                    <div class="owl-carousel learning_slider owl-theme">
                                    @foreach($publications as $publication )

                                        @if($publication->categorie->reference == 'Soudure' )

                                            <div class="item">
                                                <div class="full-width">
                                                    <div class="recent-items">
                                                        <div class="posts-list">
                                                            <div class="feed-shared-product-dt">
                                                                <div class="pdct-img crse-img-tt">
                                                                    <a href="{{ route('client.publications.show' , $publication->slug )}}">
                                                                        <img class="ft-plus-square product-bg-w bg-cyan me-0" src="{{ asset('storage/' . $publication->photo) }}" alt="">
                                                                        <div class="overlay-item">
                                                                            <div class="badge-level trnd-clr">{{ $publication->categorie->reference}}</div>
                                                                            <div class="badge-timer">services</div>
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                                <div class="author-dts pp-20">
                                                                    <a href="{{ route('client.publications.show' , $publication->slug )}}" class="job-heading pp-title">{{ $publication->titre}}</a>
                                                                    <p class="notification-text font-small-4">
                                                                        Par <a href="#" class="cmpny-dt blk-clr">{{ $publication->client->nom }} {{ $publication->client->prenom }}</a>
                                                                    </p>
                                                                    <div class="ppdt-price-sales">
                                                                        <div class="ppdt-price">
                                                                            <small>30 000 GNF - 50 000 GNF</small>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="post-meta">
                                                            <div class="job-actions">
                                                                <div class="aplcnts_15">
                                                                    <a href="{{ route('client.publications.show' , $publication->slug )}}" class="view-btn btn-hover">Detail View</a>
                                                                </div>
                                                                <div class="ppdt-sales view-btn">
                                                                    <span>330</span>
                                                                    <i class="fa fa-eye ml-2"></i>
                                                                </div>
                                                                <div class="action-btns-job">
                                                                    <a href="#" class="crt-btn crt-btn-hover me-2"><i class="fas fa-shopping-cart"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        
                                        @endif

                                    @endforeach
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="all-items">
                    <div class="product-items-list">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="learning_all_items mt-30">
                                
                                    <div class="owl_title d-flex justify-content-between">
                                        <h4>Plomberie</h4>

                                    </div>

                                    <div class="owl-carousel learning_slider owl-theme">
                                @foreach($publications as $publication )

                                @if($publication->categorie->reference == 'Plomberie' )

                                    <div class="item">
                                        <div class="full-width">
                                            <div class="recent-items">
                                                <div class="posts-list">
                                                    <div class="feed-shared-product-dt">
                                                        <div class="pdct-img crse-img-tt">
                                                            <a href="{{ route('client.publications.show' , $publication->slug ) }}">
                                                                <img class="ft-plus-square product-bg-w bg-cyan me-0" src="{{ asset('storage/' . $publication->photo) }}" alt="">
                                                                <div class="overlay-item">
                                                                    <div class="badge-level trnd-clr">{{ $publication->categorie->reference}}</div>
                                                                    <div class="badge-timer">{{ $publication->client->telephone }}</div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                        <div class="author-dts pp-20">
                                                            <a href="{{ route('client.publications.show' , $publication->slug ) }}" class="job-heading pp-title">{{ $publication->titre}}</a>
                                                            <p class="notification-text font-small-4">
                                                                Par <a href="#" class="cmpny-dt blk-clr">{{ $publication->client->nom }} {{ $publication->client->prenom }}</a>
                                                            </p>
                                                            <p class="notification-text font-small-4 pt-1 catey-group">
                                                                <a href="#" class="catey-dt">Web Development</a>
                                                            </p>
                                                            <div class="ppdt-price-sales">
                                                                <div class="ppdt-price">
                                                                    <small>30 000 GNF - 50 000 GNF</small>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="post-meta">
                                                    <div class="job-actions">
                                                        <div class="aplcnts_15">
                                                            <a href="{{ route('client.publications.show' , $publication->slug ) }}" class="view-btn btn-hover">Detail View</a>
                                                        </div>
                                                        <div class="ppdt-sales view-btn">
                                                            <span>330</span>
                                                            <i class="fa fa-eye ml-2"></i>
                                                        </div>
                                                        <div class="action-btns-job">
                                                            <a href="#" class="crt-btn crt-btn-hover me-2"><i class="fas fa-shopping-cart"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                @endif

                                @endforeach
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="all-items">
                    <div class="product-items-list">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="learning_all_items mt-30">
                                
                                    <div class="owl_title d-flex justify-content-between">
                                        <h4>Maçonnerie</h4>

                                    </div>

                                    <div class="owl-carousel learning_slider owl-theme">
                                @foreach($publications as $publication )

                                @if($publication->categorie->reference == 'Maçonnerie' )

                                    <div class="item">
                                        <div class="full-width">
                                            <div class="recent-items">
                                                <div class="posts-list">
                                                    <div class="feed-shared-product-dt">
                                                        <div class="pdct-img crse-img-tt">
                                                            <a href="{{ route('client.publications.show' , $publication->slug ) }}">
                                                                <img class="ft-plus-square product-bg-w bg-cyan me-0" src="{{ asset('storage/' . $publication->photo) }}" alt="">
                                                                <div class="overlay-item">
                                                                    <div class="badge-level trnd-clr">{{ $publication->categorie->reference}}</div>
                                                                    <div class="badge-timer">{{ $publication->client->telephone }}</div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                        <div class="author-dts pp-20">
                                                            <a href="{{ route('client.publications.show' , $publication->slug ) }}" class="job-heading pp-title">{{ $publication->titre}}</a>
                                                            <p class="notification-text font-small-4">
                                                                Par <a href="#" class="cmpny-dt blk-clr">{{ $publication->client->nom }} {{ $publication->client->prenom }}</a>
                                                            </p>
                                                            <p class="notification-text font-small-4 pt-1 catey-group">
                                                                <a href="#" class="catey-dt">Web Development</a>
                                                            </p>
                                                            <div class="ppdt-price-sales">
                                                                <div class="ppdt-price">
                                                                    <small>30 000 GNF - 50 000 GNF</small>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="post-meta">
                                                    <div class="job-actions">
                                                        <div class="aplcnts_15">
                                                            <a href="{{ route('client.publications.show' , $publication->slug ) }}" class="view-btn btn-hover">Detail View</a>
                                                        </div>
                                                        <div class="ppdt-sales view-btn">
                                                            <span>330</span>
                                                            <i class="fa fa-eye ml-2"></i>
                                                        </div>
                                                        <div class="action-btns-job">
                                                            <a href="#" class="crt-btn crt-btn-hover me-2"><i class="fas fa-shopping-cart"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                @endif

                                @endforeach
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="all-items">
                    <div class="product-items-list">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="learning_all_items mt-30">
                                
                                    <div class="owl_title d-flex justify-content-between">
                                        <h4>Mentenance</h4>

                                    </div>

                                    <div class="owl-carousel learning_slider owl-theme">
                                @foreach($publications as $publication )

                                @if($publication->categorie->reference == 'Maintenance' )

                                    <div class="item">
                                        <div class="full-width">
                                            <div class="recent-items">
                                                <div class="posts-list">
                                                    <div class="feed-shared-product-dt">
                                                        <div class="pdct-img crse-img-tt">
                                                            <a href="{{ route('client.publications.show' , $publication->slug ) }}">
                                                                <img class="ft-plus-square product-bg-w bg-cyan me-0" src="{{ asset('storage/' . $publication->photo) }}" alt="">
                                                                <div class="overlay-item">
                                                                    <div class="badge-level trnd-clr">{{ $publication->categorie->reference}}</div>
                                                                    <div class="badge-timer">{{ $publication->client->telephone }}</div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                        <div class="author-dts pp-20">
                                                            <a href="{{ route('client.publications.show' , $publication->slug ) }}" class="job-heading pp-title">{{ $publication->titre}}</a>
                                                            <p class="notification-text font-small-4">
                                                                Par <a href="#" class="cmpny-dt blk-clr">{{ $publication->client->nom }} {{ $publication->client->prenom }}</a>
                                                            </p>
                                                            <p class="notification-text font-small-4 pt-1 catey-group">
                                                                <a href="#" class="catey-dt">Web Development</a>
                                                            </p>
                                                            <div class="ppdt-price-sales">
                                                                <div class="ppdt-price">
                                                                    <small>30 000 GNF - 50 000 GNF</small>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="post-meta">
                                                    <div class="job-actions">
                                                        <div class="aplcnts_15">
                                                            <a href="{{ route('client.publications.show' , $publication->slug ) }}" class="view-btn btn-hover">Detail View</a>
                                                        </div>
                                                        <div class="ppdt-sales view-btn">
                                                            <span>330</span>
                                                            <i class="fa fa-eye ml-2"></i>
                                                        </div>
                                                        <div class="action-btns-job">
                                                            <a href="#" class="crt-btn crt-btn-hover me-2"><i class="fas fa-shopping-cart"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                @endif

                                @endforeach
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>






    
@endsection