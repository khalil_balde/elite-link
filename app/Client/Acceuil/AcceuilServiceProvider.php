<?php

namespace App\Client\Acceuil;

use Illuminate\Support\ServiceProvider;

class AcceuilServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsfrom(__DIR__.'/views', 'Acceuil');
    }
}
