@extends('client.layouts.app')

@section('client.content')
<div class="wrapper pt-0 mt-4">
    <div class="dtpgusr12">
        <div class="container">
            <div class="row">
                <aside class="col-md-4">
                    <div class="event-card rrmt-30 lgmt-30 shadow-md">
                        <div class="headtte14m">
                            <span><i class="fas fa-info"></i></span>
                            <h4>Details du soumettant</h4>
                        </div>
                        <div class="evntdt99">
                            <div class="mndtlist">
                                <div class="evntflldt4 flxcntr">
                                    <div class="hhttd14s">
                                        <i class="fas fa-user"></i>
                                    </div>
                                    <div class="ttlpple">
                                        <span>
                                            {{ $offre->client->fullName() }}
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="mndtlist">
                                <div class="evntflldt4 flxcntr">
                                    <div class="hhttd14s">
                                        <i class="fas fa-phone-alt"></i>
                                    </div>
                                    <div class="ttlpple">
                                        <span>{{ $offre->client->telephone }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="mndtlist">
                                <div class="evntflldt4 flxcntr">
                                    <div class="hhttd14s">
                                        <i class="fas fa-map-marker-alt"></i>
                                    </div>
                                    <div class="ttlpple">
                                        <span>{{ $offre->client->adresse }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="mndtlist">
                                <div class="evntflldt4 flxcntr">
                                    <div class="hhttd14s">
                                        <i class="fas fa-at"></i>
                                    </div>
                                    <div class="ttlpple">
                                        <span>{{ $offre->client->email }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="mndtlist">
                                <div class="evntflldt4 flxcntr">
                                    <div class="hhttd14s">
                                        <i class="fas fa-money-bill"></i>
                                    </div>
                                    <div class="ttlpple">
                                        <span>{{ $offre->prix }} GNF</span>
                                    </div>
                                </div>
                            </div>
                            @if ($offre->status === 0)
                            <div class="mndtlist mt-4">
                                <div class="evntflldt4 flxcntr">
                                    <div class="ttlpple w-100">
                                        <form action="{{ route('client.offres.accepter', $offre) }}" method="post">
                                            @csrf
                                            <button type="submit" class="buy-btn btn-hover">
                                                <i class="fa fa-check-circle m2-3"></i>
                                                <span>Accepter</span>
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            @endif

                        </div>
                    </div>
                </aside>
                <aside class="col-md-7">
                    <div class="card">
                        <div class="mndtlist mt-30">
                            <h4>Description</h4>
                            <div class="mndesp145 page-ddes">
                                <div class="evarticledes">
                                    <p class="mb-0" style="text-align: justify">
                                        {{ $offre->description }}   
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </div>
</div>
@endsection