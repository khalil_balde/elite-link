@extends('client.layouts.app')


@section('client.content')
<div class="wrapper pt-0 mt-4">
    <div class="content-post-job">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h3 class="cate-upload-title">Formulaire de soumission d'offres</h3>
                    <form action="{{ route('client.offres.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        
                        <div class="main-form">
                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="form_group mt-30">
                                        <label class="label25">Description</label>
                                        <textarea name="description"
                                            class="form-control form-control-lg @error ('description') is-invalid @enderror"
                                            rows="5"
                                            placeholder="Décrivez votre offre afin de permettre au client de la comprendre sans soucis et surtout d'optimiser votre chance d'être pris ...">{{ old('description') }}</textarea>
                                        @error('description')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form_group mt-30">
                                        <label class="label25">Prix de l'offre</label>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="loc_group">
                                                    <input class="form_input_1 @error ('prix_max') is-invalid @enderror"
                                                        type="number" placeholder="Ex: 12 000" name="prix_max"
                                                        value="{{ old('prix_max') }}">
                                                    <span class="slry-dt">GNF</span>
                                                    @error('prix_max')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="submit_btn">
                                        <button class="main-btn color btn-hover" type="submit">
                                            <i class="fa fa-check-square me-2"></i>
                                            <span>Publier</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-4">
                    <div class="post-job-right">
                        <img src="{{ asset('assets/images/icon-1.svg') }}" alt="">
                        <h4>Détaillez bien votre offre.</h4>
                        <p>
                            Mettez un accent sur vos compétences du domaine concernant la publiaction. <br> <br>
                            Fixez un prix raisonnable pour la tâche.
                            <br><br>
                            Decrivez votre savoir faire.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection