<?php

namespace App\Client\Offre;

use Illuminate\Support\ServiceProvider;

class OffreServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . "/views", "ClientOffre");
    }
}
