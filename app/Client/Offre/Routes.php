<?php

use App\Client\Offre\OffreController;

Route::post('offre/{offre}/accepter', [OffreController::class, 'accepter'])->name('offres.accepter');
Route::resource('offres', OffreController::class);
