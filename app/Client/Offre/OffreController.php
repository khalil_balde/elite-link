<?php

namespace App\Client\Offre;

use App\Http\Controllers\Controller;
use App\Models\Offre;
use App\Models\Publication;
use Illuminate\Http\Request;

class OffreController extends Controller
{

    public function __construct() {
        $this->middleware('auth:front');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ClientOffre::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'description' => 'required|min:20',
            'prix' => 'required|numeric'
        ]);
        $publication = Publication::query()->where('slug', $request->slug)->first();

        if ($publication) {
            Offre::query()->create([
                'description' => $request->description,
                'prix' => $request->prix,
                'client_id' => auth('front')->id(),
                'publication_id' => $request->publication->id
            ]);
            toastr()->info('L\'offre a été soumise avec succès !!!');
            return to_route('client.publications.show', compact('publication'));
        } else {
            toastr()->error('L\'offre selectionnée n\'existe pas!');
            return back();
        }
        

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Offre  $offre
     * @return \Illuminate\Http\Response
     */
    public function show(Offre $offre)
    {
        if ($offre) {
            return view('ClientOffre::details', compact('offre'));
        } else {
            toastr()->error('L\'offre selectionnée n\'existe pas !');
            return to_route('client.offres.index');
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Offre  $offre
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Publication $publication, Offre $offre)
    {
        if ($publication && $offre) {
            $offre->update([
                'description' => $request->description,
                'client_id' => auth('front')->id(),
            ]);
    
            if ($request->photo) {
                $offre->photo = $this->uploadImage($request->photo);
                $offre->save();
            }
    
            toastr()->info('L\'offre a été modifiée avec succès !!!');
            return to_route('client.publications.show', compact('publication'));
        } else {
            toastr()->error('La publication selectionnée n\'existe pas !');
            return back();
        }
    }

    public function accepter(Offre $offre)
    {
        if(auth('front')->user()->publications->contains( $offre->publication)) {
            $offre->status = 1;
            $offre->publication->status = 1;
            $offre->publication->save();
            $offre->save();
            toastr()->success('L\'offre a été accepté avec succès !!!');
            return back();
        } else {
            toastr()->error('Vous n\'êtes pas autorisés à accepter cette offre !');
            return back();
        }
    }
}
