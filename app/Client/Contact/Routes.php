<?php

use App\Client\Contact\ContactController;

Route::get('contacts', [ContactController::class, 'index'])->name('client.contact');
Route::post('contacts', [ContactController::class, 'sendEmail']);