@extends('client.layouts.app')

@section('client.content')


<div class="wrapper pt-0">
    <div class="breadcrumb-pt breadcrumb-bg ">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-title">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
                            </ol>
                        </nav>
                        <h1 class="title_text">Contact Us</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="refund-main-wrapper mt-2">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-lg-10 col-md-8">
                    <div class="new-refund-request-card main-form pt-4">
                        <form method="post" action="{{ route('client.')}}">
                        @csrf
                            <div class="container">
                                <div class="row">
                                    <div class="form_group col-md-6 mt-30">
                                        <label class="label25">Nom</label>
                                        <input class="form_input_1 @error('') is-invalid @enderror" type="text" name="nom" placeholder="" value="">
                                        @error('nom')
                                            <div class="invalid-feedback">{{ $nom }}</div>
                                        @enderror
                                    </div>
                                    <div class="form_group col-md-6 mt-30">
                                        <label class="label25">prenom</label>
                                        <input class="form_input_1 @error('prenom') is-invalid @enderror" type="text" name="prenom" placeholder="" value="">
                                        @error('prenom')
                                            <div class="invalid-feedback">{{ $prenom }}</div>
                                        @enderror
                                    </div>
                                    <div class="form_group col-md-6 mt-30">
                                        <label class="label25">E-mail</label>
                                        <input class="form_input_1 @error('email') is-invalid @enderror" type="email" name="email" placeholder="" value="">
                                        @error('email')
                                            <div class="invalid-feedback">{{ $email }}</div>
                                        @enderror
                                    </div>
                                    <div class="form_group col-md-6 mt-30">
                                        <label class="label25">telephone</label>
                                        <input class="form_input_1 @error('telephone') is-invalid @enderror" type="text" name="telephone" placeholder="" value="">
                                        @error('telephone')
                                            <div class="invalid-feedback">{{ $telephone }}</div>
                                        @enderror
                                    </div>
                                    <div class="form_group mt-30">
                                        <label class="label25">Message</label>
                                        <textarea class="form_textarea_1 @error('message') is-invalid @enderror" name="message" placeholder=""></textarea>
                                        @error('message')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="submit_btn">
                                        <button class="main-btn color btn-hover" data-ripple="">Envoyer</button>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





@stop