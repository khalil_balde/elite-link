<?php

namespace App\Client\Contact;


use App\Client\Contact\Mail\TestMail;
use App\Http\Controllers\Controller;
use App\Client\Contact\Mail\TestMail as MailTestMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Models\Contact;

class ContactController extends Controller
{
    public function __construct() {
        $this->middleware('auth:front');
    }

    public function index()
    {

        return view("Contact::index");
    }

    public function sendEmail(Request $request){
    
        $user = ["email" => "elitelink@gmail.com", "name" => "LINK"];

        $this->validate( $request , [
            'nom' => 'required',
            'prenom' => 'required',
            'email' => 'required|email',
            'telephone' => 'required',
            'message' => 'required',
        ]);

        $contact = Contact::create([

            'nom' => $request->nom,
            'prenom' => $request->prenom,
            'email' => $request->email,
            'telephone' => $request->telephone,
            'message' => $request->message,
        ]);

        $mailable = new TestMail($contact);

        Mail::to($user["email"])->send($mailable);

        toastr()->success("Votre Message a été envoyer avec succès!!!");
        return back();
    }
}
