<?php

use App\Models\Client;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paiements', function (Blueprint $table) {
            $table->id();
            $table->string('reference')->unique();
            $table->string('type')->enum(['Post', 'Souscription']);
            $table->integer('montant');
            $table->integer('nombre');
            $table->date('validite');
            $table->string('mode_paiement');
            $table->integer('status')->enum([-1, 0, 1])->default(1);

            $table->foreignIdFor(Client::class);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paiements');
    }
};
