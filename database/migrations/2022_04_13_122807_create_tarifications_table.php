<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tarifications', function (Blueprint $table) {
            $table->id();
            $table->string('type')->enum(['Post', 'Soumission']);
            $table->integer('nombre');
            $table->date('validite');
            $table->integer('prix');
            $table->integer('essai')->enum([0, 1])->default(1);
            $table->integer('redondance')->enum([0, 1])->default(0);
            $table->integer('status')->enum([0, 1])->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tarifications');
    }
};
