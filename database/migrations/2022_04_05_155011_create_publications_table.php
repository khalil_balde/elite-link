<?php

use App\Models\Categorie;
use App\Models\Client;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publications', function (Blueprint $table) {
            $table->id();

            $table->string('photo')->default('default.jpg');
            $table->string('titre');
            $table->string('slug')->unique();
            $table->text('description');
            $table->integer('prix_min');
            $table->integer('prix_max');
            $table->integer('vues')->default(0);
            $table->string('lieu');
            $table->date('date_intervention')->nullable();
            $table->integer('status')->enum([-1, 0, 1])->default(0);

            $table->foreignIdFor(Client::class);
            $table->foreignIdFor(Categorie::class);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('taches');
    }
};
