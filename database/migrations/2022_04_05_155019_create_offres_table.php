<?php

use App\Models\Client;
use App\Models\Publication;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offres', function (Blueprint $table) {
            $table->id();

            $table->text('description');
            $table->integer('prix');
            $table->integer('status')->enum([-1, 0, 1])->default(0);

            $table->foreignIdFor(Client::class);
            $table->foreignIdFor(Publication::class);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devis');
    }
};
