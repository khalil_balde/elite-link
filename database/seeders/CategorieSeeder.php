<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->truncate();

        DB::table('categories')->insert([
            'reference' => 'Informatique'
        ]);
        DB::table('categories')->insert([
            'reference' => 'Menuserie'
        ]);
        DB::table('categories')->insert([
            'reference' => 'Soudure'
        ]);
        DB::table('categories')->insert([
            'reference' => 'Plomberie'
        ]);DB::table('categories')->insert([
            'reference' => 'Maçonnerie'
        ]);
        DB::table('categories')->insert([
            'reference' => 'Maintenance'
        ]);
    }
}
