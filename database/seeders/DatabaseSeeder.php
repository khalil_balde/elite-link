<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Admin::factory(1)->create();
        \App\Models\Client::factory(50)->create();
        $this->call(CategorieSeeder::class);
        \App\Models\Publication::factory(80)->create();
        \App\Models\Offre::factory(100)->create();
        $this->call(FaqSeeder::class);
        $this->call(Information::class);
    }
}
