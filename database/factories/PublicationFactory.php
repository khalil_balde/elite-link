<?php

namespace Database\Factories;

use App\Models\Client;
use App\Models\Categorie;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Publication>
 */
class PublicationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $title = $this->faker->unique()->sentence();
        return [
            'photo' => 'default.jpg',
            'titre' => $title,
            'slug' => Str::slug($title),
            'description' => $this->faker->text(1000),
            'lieu' => $this->faker->address,
            'prix_min' => $this->faker->numberBetween(1000, 4000),
            'prix_max' => $this->faker->numberBetween(5000, 9000),
            'vues' => $this->faker->numberBetween(0, 1000),
            'date_intervention' => $this->faker->date,
            'client_id' => $this->faker->numberBetween(1, Client::count()),
            'categorie_id' => $this->faker->numberBetween(1, Categorie::count())
        ];
    }
}
