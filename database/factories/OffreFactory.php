<?php

namespace Database\Factories;

use App\Models\Client;
use App\Models\Publication;
use App\Models\Tache;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Offre>
 */
class OffreFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'description' => $this->faker->text(500),
            'prix' => $this->faker->numberBetween(1000, 9000),
            'client_id' => $this->faker->numberBetween(1, Client::query()->count()),
            'publication_id' => $this->faker->numberBetween(1, Publication::query()->count())
        ];
    }
}
