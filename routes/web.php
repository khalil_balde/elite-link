<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

// Les Rooute Pour Admin

require __DIR__ . '/../app/Admin/AdminLogin/Routes.php';
Route::group(
    [
        'prefix' => 'admin',
        'middleware' => ['auth'],
        'as' => 'admin.'
    ],
    function () {
        require __DIR__ . '/../app/Admin/Caisse/Routes.php';
        require __DIR__ . '/../app/Admin/Categorie/Routes.php';
        require __DIR__ . '/../app/Admin/Client/Routes.php';
        require __DIR__ . '/../app/Admin/Offre/Routes.php';
        require __DIR__ . '/../app/Admin/Mouvement/Routes.php';
        require __DIR__ . '/../app/Admin/Publication/Routes.php';
        require __DIR__ . '/../app/Admin/User/Routes.php';
        require __DIR__ . '/../app/Admin/Faq/Routes.php';
        require __DIR__ . '/../app/Admin/Entreprise/Routes.php';
        require __DIR__ . '/../app/Admin/Tarification/Routes.php';
        require __DIR__ . '/../app/Admin/Dashboard/Routes.php';
    }
);

// Les routes pour Client
Route::group(
    [
        'as' => 'client.',
    ],
    function () {
        
        require __DIR__ . '/../app/Client/Auth/Routes.php';
        require __DIR__ . '/../app/Client/Acceuil/Routes.php';
        require __DIR__ . '/../app/Client/Offre/Routes.php';
        require __DIR__ . '/../app/Client/Publication/Routes.php';
        require __DIR__ . '/../app/Client/Contact/Routes.php';
        require __DIR__ . '/../app/Client/Faq/Routes.php';
        require __DIR__ . '/../app/Client/Apropos/Routes.php';
        require __DIR__ . '/../app/Client/User/Routes.php';
    }
);

// Les routes de mise à jour des status
require __DIR__ . "/change-status-routes.php";
