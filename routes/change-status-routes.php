<?php

use App\Admin\User\UserController;
use App\Admin\Categorie\CategorieController;
use App\Admin\Faq\FaqController;

Route::middleware(['auth'])->group(function () {

    // User
    Route::get('users-change-status', 
                [UserController::class, 'changeStatus'])
            ->name('users.change-status');

    // Categorie
    Route::get('categories-change-status', 
                [CategorieController::class, 'changeStatus'])
            ->name('categories.change-status');

    // Categorie
    Route::get('faqs-change-status', 
                [FaqController::class, 'changeStatus'])
            ->name('categories.change-status');
});